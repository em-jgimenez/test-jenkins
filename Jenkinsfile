pipeline {
  agent any

  environment {
    // Variables temporales
    SOME_VAR = "SOME_VALUE"
    ASSEMBLY_VERSION = "Not defined"
    BUILDNAME = "Not defined"
  }

  // Etapa de build
  stages {
    // stage('Skip?') {
    //   when {
    //     changelog '.*^\\[ci skip\\] .+$'  // Commit de la forma: "[ci skip] Mensaje..."
    //   }
    //   steps {
    //     script {
    //       currentBuild.result = 'ABORTED'
    //       buildDescription "Aborted because commit message contains [skip ci]"
    //       error 'Aborting because commit message contains [skip ci]'
    //     }
          
    //   }
    // }

    stage('Build') {
      when { 
        anyOf {
          branch 'master'
          not { changelog '.*^\\[ci skip\\] .+$' } 
        }
      }
      steps {
        echo 'Building...'

        // OBtener la útlima version del archivo version.txt (sin usar)
        script {
          VERSION = sh ( script: "cat version.txt | tail -1 | cut -f1 | tr -d '\n'", returnStdout: true)
          echo "Base Version: ${VERSION}"

          if (env.BRANCH_NAME == 'master') {
            ASSEMBLY_VERESION = "${VERSION}.1.${BUILD_NUMBER}"
            BUILDNAME = "${VERSION}.1.${BUILD_NUMBER}"
          } else {
            ASSEMBLY_VERESION = "${VERSION}.0.${BUILD_NUMBER}"
            BUILDNAME = "dev-${VERSION}.0.${BUILD_NUMBER}"
          }
        }

        // Cambiamos la version del AssembyFile a la correspondiente con la build
        changeAsmVer(versionPattern: "${ASSEMBLY_VERESION}", assemblyCompany: 'ETIQMEDIA') 

        // Build names and descriptions
        buildName "${BUILDNAME}"
        buildDescription "Executed @ ${NODE_NAME}"

        // Hacemos build
        bat 'buildSimulation.bat'
        //bat 'causar_error'
      }
    }

    stage('Extract DDBB') {
      when { not { anyOf { 
        branch 'master' 
        changelog '.*^\\[ci skip\\] .+$' 
      } } }
      steps {
        echo 'Exporting database schemes and data'
        bat 'python D:/PlataformaV3-deps/getDatabase.py _DDBB/Build _DDBB/ddbb_todump.json'
        bat 'git add _DDBB/Build/**'
        bat 'git commit -m "[ci skip] Actualizar esquemas sql'
        //bat "git push --set-upstream origin ${BRANCH_NAME}"
        withCredentials([usernamePassword(credentialsId: '8caa1f2e-86e2-4221-86c8-a0e3fd7aae8c', passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                        sh('git push -f --verbose --set-upstream origin ${BRANCH_NAME}')
                    }
      }
    }

    // Para futuros tests
    stage('Test') {
      when { 
        anyOf {
          branch 'master'
          not { changelog '.*^\\[ci skip\\] .+$' } 
        }
      }
      steps {
        echo 'Testing...'
      }
    }

    // Guardamos los binarios y librerias si es release o estamos en master
    stage('Save Files') {
      
      when {
        anyOf {
          branch 'master'
          changelog '.*^\\[RELEASE\\] .+$'  // Commit de la forma: "[RELEASE] Mensaje..."
        }

      }
      steps {
        echo "Saving artifacts if all ok, we are on ${BRANCH_NAME}"
        archiveArtifacts(artifacts: 'Build/Release/**', fingerprint: true, onlyIfSuccessful: true)
        archiveArtifacts(artifacts: '_DDBB/Build/**', fingerprint: true, onlyIfSuccessful: true)
      }
    }

    // Para futuros desplieuges
    stage('Deploy') {
      when { 
        anyOf {
          branch 'master'
          not { changelog '.*^\\[ci skip\\] .+$' } 
        }
      }
      steps {
        echo 'Deploying...'
      }
    }

  }
  post {
    // Siempre
    always {
      echo 'Always'
    }
    // En caso de exito:
    success {
      echo 'Done!'
    }
    // En caso de error:
    failure {
      echo 'Sending mail...'
      emailext body: '''$DEFAULT_CONTENT''',
        recipientProviders: [culprits(), developers(), requestor()],
        subject: '''$DEFAULT_SUBJECT'''
    }
    fixed {
        echo "Back to normal"
        emailext body: '''$DEFAULT_CONTENT''',
          recipientProviders: [culprits(), developers(), requestor()],
          subject: '''$PROJECT_NAME Fixed! - Build # $BUILD_NUMBER - $BUILD_STATUS!'''
    }
  }
    
}