-- MySQL dump 10.16  Distrib 10.1.21-MariaDB, for Win32 (AMD64)
--
-- Host: 10.5.13.24    Database: 10.5.13.24
-- ------------------------------------------------------
-- Server version	10.4.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_plan_acceso_grupo`
--

DROP TABLE IF EXISTS `tb_plan_acceso_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_plan_acceso_grupo` (
  `PlanAccesoGrupo_ID` int(11) NOT NULL,
  `Plan_ID` int(11) NOT NULL,
  `AccesoGrupo_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Plan_ID`,`AccesoGrupo_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_plan_analisis`
--

DROP TABLE IF EXISTS `tb_plan_analisis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_plan_analisis` (
  `PlanAnalisis_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Plan_ID` int(11) NOT NULL,
  `PerfilAnalisis_ID` int(11) NOT NULL,
  `PerfilAlgoritmo_ID` int(11) NOT NULL,
  `Titulo` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `Tipo_ID` enum('objetos','caras','subtitulado-directo') COLLATE utf8_spanish2_ci NOT NULL,
  `Info_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'ID de la tabla relacionada donde se encuentra información extra. Esta tabla será diferente dependiendo del Tipo_ID',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PlanAnalisis_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_plan_destino`
--

DROP TABLE IF EXISTS `tb_plan_destino`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_plan_destino` (
  `PlanDestino_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Plan_ID` int(11) NOT NULL,
  `TipoDestino_ID` int(11) NOT NULL,
  `PerfilCodificacion_ID` int(11) DEFAULT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Web_ID` int(11) DEFAULT NULL,
  `Ftp_ID` int(11) DEFAULT NULL,
  `RedSocial_ID` int(11) DEFAULT NULL,
  `URL_Destino` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_ServerPath` varchar(500) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'Para Tipos de Destino ServerPath',
  `bMergeo` tinyint(4) NOT NULL DEFAULT 0,
  `MergeoFuenteReferencia` int(11) NOT NULL DEFAULT 0 COMMENT 'En caso de haber mergeo, entrada de la lista que usar como referencia',
  `ReferenciaMergeo_PlanFuente_ID` int(11) NOT NULL DEFAULT 0,
  `ListaFuentesAccion` varchar(2000) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'Fuente seleccionadas para aplicar operacion destino',
  `ListaMapeoCanales` varchar(2000) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'JSON con mapeo en formato: [ { ''Entrada'' : ''0'', ''Salida'' : ''3'', ''Ganancia'' : ''+15'' } ]',
  `bMosca` tinyint(4) NOT NULL DEFAULT 0,
  `URL_Mosca` varchar(500) COLLATE utf8_spanish_ci DEFAULT '',
  `bMoscaGuardar` tinyint(4) NOT NULL DEFAULT 0,
  `MoscaPosicion` enum('top-left','top-right','bottom-left','bottom-right') COLLATE utf8_spanish_ci DEFAULT NULL,
  `bEncapsulado` tinyint(4) DEFAULT 0,
  `EncapsuladoTipo` enum('hls','dash','hls-dash') COLLATE utf8_spanish_ci DEFAULT NULL,
  `EncapsuladoDuracion` int(11) DEFAULT NULL COMMENT 'Duracion en segundos de cada chunk',
  `bNormalizarAudio` tinyint(4) NOT NULL DEFAULT 0,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `DVRDuracion` int(11) DEFAULT 0,
  `bSegmentAudio` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`PlanDestino_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=684 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1236;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_plan_exportacion`
--

DROP TABLE IF EXISTS `tb_plan_exportacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_plan_exportacion` (
  `PlanExportacion_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Plan_ID` int(11) NOT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Filename` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ManagerExportacion_ID` enum('ninguno','indexacion','transcodificacion') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'ninguno',
  `PerfilExportacion_ID` int(11) DEFAULT NULL COMMENT 'El manager seleccionado indicará de que tabla tb_perfil_exportacion_ hay que coger la informacion',
  `FtpExportacion_ID` int(11) DEFAULT NULL,
  `URL_Exportacion` varchar(500) COLLATE utf8_spanish_ci DEFAULT '',
  `bGuardarCarpetaOrigen` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Si es true, se guardará el resultado en la misma carpeta que el video original',
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PlanExportacion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=386 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_plan_fuente`
--

DROP TABLE IF EXISTS `tb_plan_fuente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_plan_fuente` (
  `PlanFuente_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Plan_ID` int(11) NOT NULL,
  `TipoFuente_ID` int(11) NOT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Origen_ID` int(11) DEFAULT NULL,
  `Grupo_ID` int(11) DEFAULT NULL COMMENT 'Representa a SDI, TDT, IP, Audio',
  `PerfilCaptura_ID` int(11) DEFAULT NULL,
  `TipoCaptura` enum('hls','ts','dash','hls-dash','ninguno') COLLATE utf8_spanish_ci DEFAULT 'hls',
  `EncapsuladoDuracion` int(11) DEFAULT 0,
  `RedSocial_ID` int(11) DEFAULT NULL,
  `Web_ID` int(11) DEFAULT NULL,
  `Ftp_ID` int(11) DEFAULT NULL COMMENT 'Los detalles de operativa en FTP se indicarÃ¡n en la configuracion asociada mediante FolderConfig_ID',
  `URL_Fuente` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Si Web_ID indica Streaming de origen, Si FTP_ID indica directorio remoto en FTP',
  `URL_LocalFile` text COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Toda la info necesaria para recrear el upload del fichero',
  `URL_ServerFile` text COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Si URL_LocalFile contiene informacion de upload, aqui encontraremos la ruta limpia al fichero subido, siempre deberÃ¡ comenzar por /uploads/',
  `URL_ServerPath` varchar(500) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'Los detalles de operativa en ServerPath se indicarÃ¡n en la configuracion asociada mediante FolderConfig_ID',
  `FolderConfig_ID` int(11) NOT NULL COMMENT 'Indica como operar con la carpeta, que estarÃ¡ vinculada a un URL_ServerPath o un FTP_ID',
  `ListaSeleccion` text COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Lista de items seleccionados separados por pipes',
  `ListaMapeoCanales` varchar(2000) COLLATE utf8_spanish_ci DEFAULT '',
  `Filename` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bBorrarTrasProceso` tinyint(4) NOT NULL DEFAULT 0,
  `bIncluirEnCarpetaCaliente` tinyint(4) NOT NULL DEFAULT 0,
  `DVRDuracion` int(11) DEFAULT 0,
  `bSegmentAudio` tinyint(4) NOT NULL DEFAULT 0,
  `ExportacionPeriodica_ServerPath` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Path donde se exportará el clip',
  `ExportacionPeriodica_Duracion` int(11) DEFAULT 0 COMMENT 'Tiempo en segundos de la duracion del clip',
  `ExportacionPeriodica_Solape` int(11) DEFAULT 0 COMMENT 'Tiempo en segundos de solape entre un clip y otro',
  `MinDurationSeconds` int(11) DEFAULT 0,
  PRIMARY KEY (`PlanFuente_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1734 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=682;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_plan_notificacion`
--

DROP TABLE IF EXISTS `tb_plan_notificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_plan_notificacion` (
  `PlanNotificacion_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Plan_ID` int(11) NOT NULL,
  `TipoNotificacion_ID` enum('ninguna','email','snmp') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'ninguna',
  `TiempoNotificacion` enum('inicio','final','ambas') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'final',
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Notificacion` varchar(500) COLLATE utf8_spanish_ci DEFAULT '',
  `ListaDestinatarios` varchar(2000) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'Destinatarios separados por pipes',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PlanNotificacion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=364;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_plan_streaming`
--

DROP TABLE IF EXISTS `tb_plan_streaming`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_plan_streaming` (
  `PlanStreaming_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Plan_ID` int(11) NOT NULL,
  `EmisionPlataforma` enum('wowza','youtube','facebook','instagram','periscope','twitter','url') COLLATE utf8_spanish_ci DEFAULT NULL,
  `EmisionStreaming_ID` int(11) NOT NULL COMMENT 'Hace las veces de "TipoStreaming_ID"',
  `PerfilCodificacion_ID` int(11) DEFAULT NULL,
  `URL_EmisionPlan` varchar(500) COLLATE utf8_spanish_ci DEFAULT '',
  `Key_EmisionPlan` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `User_EmisionPlan` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Pass_EmisionPlan` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PlanFuente_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'Fuente seleccionada como origen de la emision',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PlanStreaming_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=409 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=399;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_planificacion`
--

DROP TABLE IF EXISTS `tb_planificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_planificacion` (
  `Plan_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `Tipologia` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Indica las caracteristicas del WF mediante iconos',
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Version verbalizada de Tipologia para describir las caracteristicas del WF',
  `FechaIni` datetime DEFAULT NULL,
  `FechaFin` datetime DEFAULT NULL,
  `DiasSemana` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `HoraIni` time DEFAULT NULL,
  `HoraFin` time DEFAULT NULL,
  `bFragmentoUnico` tinyint(4) NOT NULL DEFAULT 0,
  `FragmentoSegundos` int(11) DEFAULT 0,
  `bCapturaFoto` tinyint(4) NOT NULL DEFAULT 0,
  `FragmentoSegundosFoto` int(11) DEFAULT 0,
  `bCapturaLoop` tinyint(4) NOT NULL DEFAULT 0,
  `FragmentoSegundosLoop` int(11) DEFAULT 0,
  `PerfilAnalisis_ID` int(11) DEFAULT NULL,
  `PerfilIndexacion_ID` int(11) DEFAULT NULL,
  `FechaAlta` datetime NOT NULL DEFAULT current_timestamp(),
  `Estado_ID` enum('creating','running','waiting','deleted') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'creating',
  `bActivo` tinyint(4) NOT NULL DEFAULT 0,
  `Prioridad` int(11) NOT NULL DEFAULT 1,
  `Usuario_ID` int(11) NOT NULL COMMENT 'Usuario que ha generado el workflow',
  `Modo` enum('captura-video','captura-audio','fichero-video','fichero-audio','fichero-foto') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'captura-video',
  `PerfilExportacion_ID` int(11) DEFAULT NULL,
  `FtpExportacion_ID` int(11) DEFAULT NULL,
  `URL_Exportacion` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bPreparado` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'En caso de ser true, el Workflow se ha preparado con anterioridad, y ya ha generado las cosas necesarias',
  PRIMARY KEY (`Plan_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=681 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=489;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-08 11:49:18
