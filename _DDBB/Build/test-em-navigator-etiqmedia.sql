-- MySQL dump 10.16  Distrib 10.1.21-MariaDB, for Win32 (AMD64)
--
-- Host: 10.5.13.24    Database: 10.5.13.24
-- ------------------------------------------------------
-- Server version	10.4.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_canal`
--

DROP TABLE IF EXISTS `tb_canal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_canal` (
  `Canal_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ChannelID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Thumbnail` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Canal_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=390;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_channel_broadcast`
--

DROP TABLE IF EXISTS `tb_channel_broadcast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_channel_broadcast` (
  `Channel_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `Title` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Company` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `VideoDB_Name` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `VideoDB_Index` int(11) DEFAULT 0,
  `VideoDB_ServerURL` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `VideoDB_ServerUploadEntry` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bRadio` tinyint(4) NOT NULL DEFAULT 0,
  `URL_Thumbnail` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Channel_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=273;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-08 11:49:18
