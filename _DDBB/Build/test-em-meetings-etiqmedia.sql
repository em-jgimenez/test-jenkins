-- MySQL dump 10.16  Distrib 10.1.21-MariaDB, for Win32 (AMD64)
--
-- Host: 10.5.13.24    Database: 10.5.13.24
-- ------------------------------------------------------
-- Server version	10.4.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `-tb_participante_grupo`
--

DROP TABLE IF EXISTS `-tb_participante_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `-tb_participante_grupo` (
  `Portal_ID` int(11) NOT NULL DEFAULT 1,
  `Grupo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Tipo_ID` int(11) DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Grupo_ID`),
  UNIQUE KEY `Grupo_ID_IND` (`Grupo_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `-tb_participante_grupo_permiso`
--

DROP TABLE IF EXISTS `-tb_participante_grupo_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `-tb_participante_grupo_permiso` (
  `Grupo_ID` int(11) NOT NULL,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Permiso_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Grupo_ID`,`Portal_ID`,`Permiso_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `-tb_participante_permiso`
--

DROP TABLE IF EXISTS `-tb_participante_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `-tb_participante_permiso` (
  `Portal_ID` int(11) NOT NULL DEFAULT 1,
  `Permiso_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Tipo_ID` int(11) DEFAULT NULL COMMENT 'Descarga, Compartir, etc...',
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Permiso_ID`),
  UNIQUE KEY `Permiso_ID_IND` (`Permiso_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `-tb_participante_usuario`
--

DROP TABLE IF EXISTS `-tb_participante_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `-tb_participante_usuario` (
  `Portal_ID` int(11) NOT NULL DEFAULT 1,
  `Usuario_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Login` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Password` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Email` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `RolParticipante_ID` enum('8','9','10') COLLATE utf8_spanish_ci NOT NULL DEFAULT '10' COMMENT 'Moderador, Editor, Invitado',
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `Nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaAlta` datetime DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `FechaUltimoparticipante` datetime DEFAULT NULL,
  `Estado_ID` enum('-1','0','1') COLLATE utf8_spanish_ci DEFAULT '-1' COMMENT '-1: DESCONOCIDO, 0: DESCONECTADO, 1: CONECTADO',
  `PasswordVer` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `Origen` varchar(255) COLLATE utf8_spanish_ci DEFAULT 'MANUAL',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `Idioma` enum('en','es') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'es',
  `Movil` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `bConfirmado` tinyint(4) NOT NULL DEFAULT 1,
  `URL_Avatar` varchar(500) COLLATE utf8_spanish_ci DEFAULT '',
  `DATA_Avatar` blob DEFAULT NULL,
  `bBaja` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Usuario_ID`),
  KEY `Portal_ID_IND` (`Portal_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `-tb_participante_usuario_grupo`
--

DROP TABLE IF EXISTS `-tb_participante_usuario_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `-tb_participante_usuario_grupo` (
  `Portal_ID` int(11) NOT NULL DEFAULT 1,
  `Usuario_ID` int(11) NOT NULL,
  `Grupo_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Usuario_ID`,`Grupo_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `-tb_participante_usuario_permiso`
--

DROP TABLE IF EXISTS `-tb_participante_usuario_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `-tb_participante_usuario_permiso` (
  `Usuario_ID` int(11) NOT NULL,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Permiso_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Usuario_ID`,`Portal_ID`,`Permiso_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_firma_fichero`
--

DROP TABLE IF EXISTS `tb_firma_fichero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_firma_fichero` (
  `Portal_ID` int(11) DEFAULT NULL,
  `Firma_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Sesion_ID` int(11) DEFAULT NULL,
  `Video_ID` int(11) DEFAULT NULL,
  `Recurso_ID` int(11) DEFAULT NULL,
  `Tipo_ID` enum('video','recurso') COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Tipo de firma: Video, PDF',
  `Token` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PostEndpoint` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Name` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `MimeType` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `DownloadURL` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Signed` datetime DEFAULT NULL,
  `HashAlgorithm` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Hash` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `StampSign` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FirmaURL` varchar(255) CHARACTER SET utf8 DEFAULT 'etiqsigner://',
  `Base64JSON` text CHARACTER SET utf8 DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `Firmado_XML` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_PDF` blob DEFAULT NULL,
  `Firmado_Documento` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Fecha` datetime DEFAULT NULL,
  `Firmado_Sujeto` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Emisor` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Algoritmo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_ValidoDesde` datetime DEFAULT NULL,
  `Firmado_ValidoHasta` datetime DEFAULT NULL,
  `Firmado_Validez` tinyint(4) DEFAULT 1,
  `Firmador_Data` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Uppy_Data` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Firma_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_firma_fichero_multiple`
--

DROP TABLE IF EXISTS `tb_firma_fichero_multiple`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_firma_fichero_multiple` (
  `FirmaMultiple_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Firma_ID` int(11) unsigned NOT NULL,
  `Sesion_ID` int(11) DEFAULT NULL,
  `Video_ID` int(11) DEFAULT NULL,
  `Recurso_ID` int(11) DEFAULT NULL,
  `Tipo_ID` enum('video','recurso') COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Tipo de firma: Video, PDF',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `Firmado_XML` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_PDF` blob DEFAULT NULL,
  `Firmado_Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Fecha` datetime DEFAULT NULL,
  `Firmado_Sujeto` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Emisor` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Algoritmo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_ValidoDesde` datetime DEFAULT NULL,
  `Firmado_ValidoHasta` datetime DEFAULT NULL,
  `Firmado_Validez` tinyint(4) DEFAULT 1,
  `Firmador_Data` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `Signed` datetime DEFAULT NULL,
  PRIMARY KEY (`FirmaMultiple_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_participante_invitado`
--

DROP TABLE IF EXISTS `tb_participante_invitado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_participante_invitado` (
  `Portal_ID` int(11) NOT NULL DEFAULT 501,
  `Invitado_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Login` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `Password` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `Email` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `RolParticipante_ID` enum('8','9','10') COLLATE utf8_spanish_ci NOT NULL DEFAULT '10' COMMENT 'Moderador, Editor, Invitado',
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `Nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaAlta` datetime DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `FechaUltimoAcceso` datetime DEFAULT NULL,
  `Estado_ID` enum('-1','0','1') COLLATE utf8_spanish_ci DEFAULT '-1' COMMENT '-1: DESCONOCIDO, 0: DESCONECTADO, 1: CONECTADO',
  `PasswordVer` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `Origen` varchar(255) COLLATE utf8_spanish_ci DEFAULT 'MANUAL',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `Idioma` enum('en','es') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'es',
  `NIF` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `bConfirmado` tinyint(4) NOT NULL DEFAULT 1,
  `URL_Avatar` varchar(500) COLLATE utf8_spanish_ci DEFAULT '',
  `DATA_Avatar` blob DEFAULT NULL,
  `bBaja` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Invitado_ID`),
  KEY `Portal_ID_IND` (`Portal_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_sesion`
--

DROP TABLE IF EXISTS `tb_sesion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sesion` (
  `Sesion_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Usuario_ID` int(11) DEFAULT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaIni` datetime DEFAULT NULL,
  `FechaFin` datetime DEFAULT NULL,
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Modo_ID` enum('telematica','presencial','mixta') COLLATE utf8_spanish_ci DEFAULT 'telematica',
  `Organo` enum('junta','secretaria','ayuntamiento') COLLATE utf8_spanish_ci DEFAULT NULL,
  `Estado_ID` enum('convocatoria','enviada','cursando','procesando','pendiente-firma','finalizada','publicada') COLLATE utf8_spanish_ci DEFAULT 'convocatoria',
  `URL_Thumbnail` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_SesionActa` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Compilacion manual de los URL_BorradorActa de todos los videos de la sesion',
  `Token` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `MeetPass` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bPublicado` tinyint(4) unsigned NOT NULL DEFAULT 0,
  `Uppy_Data` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `Expediente` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Emision_YT` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Consumo_WOWZA` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Accion_WF` enum('emision','grabacion','emision-grabacion') COLLATE utf8_spanish_ci DEFAULT 'emision',
  `Filename_WF` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Sesion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_sesion_ordendeldia`
--

DROP TABLE IF EXISTS `tb_sesion_ordendeldia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sesion_ordendeldia` (
  `OrdenDelDia_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Template_ID` int(11) NOT NULL DEFAULT 1,
  `Sesion_ID` int(11) DEFAULT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Entradilla` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Subtitulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaAlta` datetime DEFAULT current_timestamp(),
  `PieCiudad` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PieTitulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PieFirma` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Organo` enum('junta','secretaria','ayuntamiento') COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Template` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Template que se utilizará para generar los documentos',
  `URL_Documento` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Template con las secciones que se utilizará para generar los documentos',
  `Uppy_Data` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Info relativa a URL_Template',
  `bActivo` tinyint(4) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`OrdenDelDia_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_sesion_ordendeldia_punto`
--

DROP TABLE IF EXISTS `tb_sesion_ordendeldia_punto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sesion_ordendeldia_punto` (
  `ODD_Punto_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ODD_Seccion_ID` int(11) NOT NULL,
  `OrdenDelDia_ID` int(11) NOT NULL,
  `Sesion_ID` int(11) NOT NULL,
  `Titulo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `TiempoIni` int(11) DEFAULT NULL,
  `TiempoFin` int(11) DEFAULT NULL,
  `URL_Fichero` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Uppy_Data` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`ODD_Punto_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_sesion_ordendeldia_seccion`
--

DROP TABLE IF EXISTS `tb_sesion_ordendeldia_seccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sesion_ordendeldia_seccion` (
  `ODD_Seccion_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `OrdenDelDia_ID` int(11) NOT NULL,
  `Sesion_ID` int(11) NOT NULL,
  `Titulo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `TiempoIni` int(11) DEFAULT NULL,
  `TiempoFin` int(11) DEFAULT NULL,
  `URL_Fichero` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Uppy_Data` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`ODD_Seccion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_sesion_participante`
--

DROP TABLE IF EXISTS `tb_sesion_participante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sesion_participante` (
  `Participante_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(11) DEFAULT 501,
  `Sesion_ID` int(11) NOT NULL,
  `Usuario_ID` int(11) DEFAULT NULL,
  `Invitado_ID` int(11) DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bModerador` tinyint(4) NOT NULL DEFAULT 0,
  `bEditor` tinyint(4) NOT NULL DEFAULT 0,
  `bConectado` tinyint(4) NOT NULL DEFAULT 0,
  `FechaUltimaConexion` datetime DEFAULT NULL,
  PRIMARY KEY (`Participante_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=564 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1638;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_sesion_recurso`
--

DROP TABLE IF EXISTS `tb_sesion_recurso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sesion_recurso` (
  `Portal_ID` int(11) DEFAULT NULL,
  `Recurso_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Sesion_ID` int(11) NOT NULL,
  `URL_Fichero` varchar(1000) COLLATE utf8_spanish_ci NOT NULL,
  `Tipo_ID` enum('documento','imagen','borrador-acta','orden-del-dia','preparado-firma','sesion-acta') COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Tipo de fichero: Imagen, PDF, Subtitulo, Documento, Etc.',
  `Titulo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 0,
  `Hash` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bFirmado` tinyint(4) NOT NULL DEFAULT 0,
  `FechaFirma` datetime DEFAULT NULL,
  `Uppy_Data` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Recurso_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_sesion_video`
--

DROP TABLE IF EXISTS `tb_sesion_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sesion_video` (
  `Sesion_ID` int(11) NOT NULL,
  `Video_ID` int(11) NOT NULL,
  PRIMARY KEY (`Sesion_ID`,`Video_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_sesion_video_grupo`
--

DROP TABLE IF EXISTS `tb_sesion_video_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sesion_video_grupo` (
  `PermisoSesion_ID` int(11) NOT NULL,
  `PermisoVideo_ID` int(11) NOT NULL,
  `AccesoGrupo_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PermisoSesion_ID`,`PermisoVideo_ID`,`AccesoGrupo_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_template_documento`
--

DROP TABLE IF EXISTS `tb_template_documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_template_documento` (
  `Template_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Entradilla` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Subtitulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaAlta` datetime DEFAULT current_timestamp(),
  `PieCiudad` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PieTitulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PieFirma` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Organo` enum('junta','secretaria','ayuntamiento') COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Template` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Template que se utilizará para generar los documentos',
  `File_Documento` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Formato para el nombre de los ficheros a generar',
  `Uppy_Data` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Info relativa a URL_Template',
  `bActivo` tinyint(4) unsigned NOT NULL DEFAULT 1,
  `URL_Logo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Logo para incluir en Template que se utilizará para generar los documentos',
  `LogoPosicion` enum('top-left','top-center','top-right') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Template_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video`
--

DROP TABLE IF EXISTS `tb_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video` (
  `Video_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaEmision` datetime DEFAULT NULL,
  `SOLR_videoID` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Video` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Thumbnail` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Organ_ID` int(11) DEFAULT NULL,
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_visualizationRights` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_BorradorActa` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Hash` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bFirmado` tinyint(4) NOT NULL DEFAULT 0,
  `FechaFirma` datetime DEFAULT NULL,
  `bPublicado` tinyint(4) NOT NULL DEFAULT 0,
  `FechaPublicacion` datetime DEFAULT NULL,
  `Uppy_Data` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `DropboxFileName` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Video_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2773 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-08 11:49:18
