-- MySQL dump 10.16  Distrib 10.1.21-MariaDB, for Win32 (AMD64)
--
-- Host: 10.5.13.24    Database: 10.5.13.24
-- ------------------------------------------------------
-- Server version	10.4.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `tb_template_documento`
--

LOCK TABLES `tb_template_documento` WRITE;
/*!40000 ALTER TABLE `tb_template_documento` DISABLE KEYS */;
INSERT INTO `tb_template_documento` VALUES (1,'TEST SESIÓN DEL EXCMO. AYUNTAMIENTO','Haciendo uso de las atribuciones que me confiere el art. 124.4.d) de la Ley 7/1.985, de 2 de abril, Regulado¬ra de las Bases del Régimen Local y de conformidad con lo preceptuado en los arts. 46 y 126 de la misma, he dispuesto convocar a todos los miembros de la Junta de Gobierno Local para que concurran el próximo jueves, día 10 de octubre de 2019, a las ocho horas y treinta minutos, en la Sala de Comisiones de la Casa Consistorial, al objeto de cele¬brar sesión ordinaria con el siguiente:','ORDEN DEL DÍA','2021-11-12 00:00:00','Zaragoza','Firmado','El secretario correspondiente','ayuntamiento',NULL,'OrdenDelDia',NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `tb_template_documento` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-08 11:49:18
