-- MySQL dump 10.16  Distrib 10.1.21-MariaDB, for Win32 (AMD64)
--
-- Host: 10.5.13.24    Database: 10.5.13.24
-- ------------------------------------------------------
-- Server version	10.4.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `base_categoria`
--

DROP TABLE IF EXISTS `base_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_categoria` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Categoria_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CodigoAPP` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `CategoriaPadre_ID` int(11) NOT NULL DEFAULT 0,
  `Foto1_ID` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `ERP_ID` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Categoria_ID`),
  UNIQUE KEY `Categoria_ID_IND_U` (`Categoria_ID`),
  KEY `Foto1_ID_ind` (`Foto1_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_ccaa_es`
--

DROP TABLE IF EXISTS `base_ccaa_es`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_ccaa_es` (
  `CCAA_ID` tinyint(4) NOT NULL,
  `Titulo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `COD` char(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`CCAA_ID`),
  UNIQUE KEY `CCAA_ID_IND_U` (`CCAA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=862;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_language_code_i18n`
--

DROP TABLE IF EXISTS `base_language_code_i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_language_code_i18n` (
  `3Letras_COD` char(3) CHARACTER SET utf8 NOT NULL COMMENT 'ISO 639-2 Code',
  `2Letras_COD` varchar(2) CHARACTER SET utf8 DEFAULT NULL COMMENT 'ISO 639-1 Code',
  `Titulo_ES` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Titulo_EN` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Titulo_FR` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`3Letras_COD`),
  KEY `2Letras_COD` (`2Letras_COD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_municipio_es`
--

DROP TABLE IF EXISTS `base_municipio_es`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_municipio_es` (
  `Municipio_ID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `Provincia_COD` char(2) COLLATE utf8_spanish_ci NOT NULL,
  `Municipio_COD` char(3) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Codigo de muncipio DENTRO de la provincia, campo no unico',
  `DC` int(11) NOT NULL COMMENT 'Digito Control. El INE no revela como se calcula, secreto nuclear.',
  `Titulo` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`Municipio_ID`),
  UNIQUE KEY `Municipio_ID_IND_U` (`Municipio_ID`),
  KEY `Provincia_COD_IND` (`Provincia_COD`)
) ENGINE=InnoDB AUTO_INCREMENT=8117 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=52;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_pais`
--

DROP TABLE IF EXISTS `base_pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_pais` (
  `Pais_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(64) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Alpha_2` varchar(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Alpha_3` varchar(3) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 0,
  `Orden` int(11) NOT NULL DEFAULT 0,
  `Continente_ID` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Pais_ID`),
  KEY `countries_id` (`Pais_ID`),
  KEY `IDX_COUNTRIES_NAME` (`Nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=72;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_pais_es`
--

DROP TABLE IF EXISTS `base_pais_es`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_pais_es` (
  `Pais_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Alpha_2` varchar(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Alpha_3` varchar(3) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bActivo` tinyint(4) DEFAULT 0,
  `Orden` int(11) NOT NULL DEFAULT 0,
  `Continente_ID` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Pais_ID`),
  KEY `countries_id` (`Pais_ID`),
  KEY `IDX_COUNTRIES_NAME` (`Titulo`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=72;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_pais_iso_3166_1_alpha_2`
--

DROP TABLE IF EXISTS `base_pais_iso_3166_1_alpha_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_pais_iso_3166_1_alpha_2` (
  `Pais_COD` char(2) COLLATE utf8_spanish_ci NOT NULL COMMENT 'ISO 3166-1 alpha-2 code',
  `Titulo` varchar(70) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'English name of the country',
  `TLD` varchar(7) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Top Level Domain(s). UK has two TLDs!',
  PRIMARY KEY (`Pais_COD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=65;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_provincia`
--

DROP TABLE IF EXISTS `base_provincia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_provincia` (
  `Provincia_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Pais_ID` int(11) DEFAULT 0,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Provincia_ID`),
  UNIQUE KEY `Provincia_ID_IND_U` (`Provincia_ID`),
  KEY `Pais_ID` (`Pais_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3390 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=53;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_provincia_es`
--

DROP TABLE IF EXISTS `base_provincia_es`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_provincia_es` (
  `Provincia_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Provincia_COD` char(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CCAA_ID` tinyint(4) DEFAULT NULL,
  `World_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Provincia_ID`),
  UNIQUE KEY `Provincia_ID_IND_U` (`Provincia_ID`),
  KEY `CCAA_ID_IND` (`CCAA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=315;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_tipo`
--

DROP TABLE IF EXISTS `base_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_tipo` (
  `Tipo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CategoriaPadre_ID` int(11) NOT NULL DEFAULT 0,
  `CodigoAPP` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Codigo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) DEFAULT 99999,
  `Foto1_ID` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Tipo_ID`),
  UNIQUE KEY `Tipo_Ind_ID_U` (`Tipo_ID`),
  KEY `bActivo_IND` (`bActivo`),
  KEY `Categoria_padre_ID` (`CategoriaPadre_ID`),
  KEY `Orden` (`Orden`)
) ENGINE=InnoDB AUTO_INCREMENT=1122 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_grupo`
--

DROP TABLE IF EXISTS `tb_acceso_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_grupo` (
  `Portal_ID` int(11) NOT NULL DEFAULT 1,
  `AccesoGrupo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Tipo_ID` int(11) DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`AccesoGrupo_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`AccesoGrupo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=4096;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_grupo_permiso`
--

DROP TABLE IF EXISTS `tb_acceso_grupo_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_grupo_permiso` (
  `AccesoGrupo_ID` int(11) NOT NULL,
  `AccesoPortal_ID` int(11) NOT NULL DEFAULT 0,
  `AccesoPermiso_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`AccesoGrupo_ID`,`AccesoPortal_ID`,`AccesoPermiso_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1638;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_grupo_video`
--

DROP TABLE IF EXISTS `tb_acceso_grupo_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_grupo_video` (
  `Portal_ID` int(11) NOT NULL DEFAULT 1001,
  `Video_ID` int(11) NOT NULL,
  `Grupo_ID` int(11) NOT NULL,
  PRIMARY KEY (`Video_ID`,`Grupo_ID`,`Portal_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_log`
--

DROP TABLE IF EXISTS `tb_acceso_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_log` (
  `AccesoLog_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Usuario_ID` int(11) NOT NULL DEFAULT 0,
  `Portal_ID` int(11) NOT NULL DEFAULT 1,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `IP` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `Fecha` datetime DEFAULT NULL,
  `Login` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Password` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Acceso` varchar(32) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `UserAgent` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SO` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Navegador` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bError` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`AccesoLog_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2489 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=486;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_permiso`
--

DROP TABLE IF EXISTS `tb_acceso_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_permiso` (
  `Portal_ID` int(11) NOT NULL DEFAULT 1,
  `AccesoPermiso_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Tipo_ID` int(11) DEFAULT NULL COMMENT 'Descarga, Compartir, etc...',
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`AccesoPermiso_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`AccesoPermiso_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=264 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=3276;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_usuario`
--

DROP TABLE IF EXISTS `tb_acceso_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_usuario` (
  `Portal_ID` int(11) NOT NULL DEFAULT 100 COMMENT 'Portal por defecto para redireccion tras login de usuario',
  `Usuario_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Login` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Password` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Email` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `RolAcceso_ID` enum('2','3','4','5','6','7') COLLATE utf8_spanish_ci NOT NULL DEFAULT '7' COMMENT 'Administrador, Gestor, Consulta, Usuario',
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `Nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaAlta` datetime DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `FechaUltimoAcceso` datetime DEFAULT NULL,
  `Estado_ID` enum('-1','0','1') COLLATE utf8_spanish_ci DEFAULT '-1' COMMENT '-1: DESCONOCIDO, 0: DESCONECTADO, 1: CONECTADO',
  `PasswordVer` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `Origen` varchar(255) COLLATE utf8_spanish_ci DEFAULT 'MANUAL' COMMENT 'Se ha creado de forma MANUAL en la plataforma o desde API de VideoActas',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `Idioma` enum('en','es') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'es',
  `NIF` varchar(20) COLLATE utf8_spanish_ci DEFAULT '',
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `bConfirmado` tinyint(4) NOT NULL DEFAULT 1,
  `URL_Avatar` varchar(500) COLLATE utf8_spanish_ci DEFAULT '',
  `DATA_Avatar` blob DEFAULT NULL,
  `bBaja` tinyint(4) NOT NULL DEFAULT 0,
  `Movil` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Telefono` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `API_ID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Identificador en API de Usuarios integrada (si procede)',
  `FechaExpiracion` datetime DEFAULT NULL,
  PRIMARY KEY (`Usuario_ID`),
  KEY `Portal_ID_IND` (`Portal_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=352 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=2730;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_usuario_grupo`
--

DROP TABLE IF EXISTS `tb_acceso_usuario_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_usuario_grupo` (
  `AccesoPortal_ID` int(11) NOT NULL,
  `AccesoUsuario_ID` int(11) NOT NULL,
  `AccesoGrupo_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`AccesoPortal_ID`,`AccesoUsuario_ID`,`AccesoGrupo_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1820;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_usuario_permiso`
--

DROP TABLE IF EXISTS `tb_acceso_usuario_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_usuario_permiso` (
  `AccesoUsuario_ID` int(11) NOT NULL,
  `AccesoPortal_ID` int(11) NOT NULL DEFAULT 0,
  `AccesoPermiso_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`AccesoUsuario_ID`,`AccesoPortal_ID`,`AccesoPermiso_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_aspect_ratio`
--

DROP TABLE IF EXISTS `tb_aspect_ratio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aspect_ratio` (
  `AspectRatio_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `AspectRatioID` varchar(255) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'Campo Name del XML',
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT '' COMMENT '16/9',
  `Denominador` int(11) NOT NULL DEFAULT 9,
  `Numerador` int(11) NOT NULL DEFAULT 16,
  `Factor` decimal(6,4) DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`AspectRatio_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=8192;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_audio_codec`
--

DROP TABLE IF EXISTS `tb_audio_codec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_audio_codec` (
  `AudioCodec_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `AudioCodecID` varchar(255) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'Campo Name del XML',
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'AAC',
  `LibreriaCodec` varchar(255) COLLATE utf8_spanish_ci DEFAULT 'aac',
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`AudioCodec_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=3276;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_canal`
--

DROP TABLE IF EXISTS `tb_canal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_canal` (
  `Canal_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ChannelID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Thumbnail` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Canal_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=381;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_capture_plan`
--

DROP TABLE IF EXISTS `tb_capture_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_capture_plan` (
  `CapturePlan_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Server_ID` int(11) DEFAULT NULL,
  `Name` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Estado` int(11) NOT NULL DEFAULT 0 COMMENT 'Estado. Se usa una representación Bit-Wise: 0 - Idle. 1 << 0 - Distributing. 1 << 1 - Capturing. 1 << 2 - Recording.',
  `Planificacion_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `TipoCaptura` enum('hls','ts','dash','hls-dash','image','ninguno') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'ninguno',
  `TipoPlanCaptura` enum('none','periodical_image') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'none',
  `EncapsulationTime` int(11) NOT NULL DEFAULT -1,
  `DestinoEncapsulationTime` int(11) NOT NULL DEFAULT -1,
  `TiempoIni` datetime NOT NULL DEFAULT current_timestamp(),
  `TiempoFin` datetime NOT NULL DEFAULT current_timestamp(),
  `DispositivoInput_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `CanalesSeleccionadosTDT` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Es una lista con {muxID}#{canal1}#{canal2}...',
  `OutputFolderPath` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `OutputFileName` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `OutputFileExtension` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ChunkSize` time DEFAULT NULL,
  `bUploadChunks` tinyint(4) NOT NULL DEFAULT 0,
  `bGenerateThumbnails` tinyint(4) NOT NULL DEFAULT 0,
  `bExtractImportantFrames` tinyint(4) NOT NULL DEFAULT 0,
  `Portal_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `AudioMappings` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Lista con los mapeos de audio, separados por pipes |',
  `LastUpdateTime` datetime NOT NULL DEFAULT current_timestamp(),
  `bCapturaLoop` tinyint(4) NOT NULL DEFAULT 0,
  `FragmentoSegundosLoop` int(11) NOT NULL DEFAULT 0,
  `PlanFuente_ID` int(11) unsigned NOT NULL,
  `SourceTypeID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `InputURL` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `DestinoDVRDuracion` int(11) DEFAULT 0,
  `DVRDuracion` int(11) DEFAULT 0,
  `CanalPID` int(11) NOT NULL DEFAULT -1,
  `ExportacionPeriodica_ServerPath` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Path donde se exportará el clip',
  `ExportacionPeriodica_Duracion` int(11) DEFAULT 0 COMMENT 'Tiempo en segundos de la duracion del clip',
  `ExportacionPeriodica_Solape` int(11) DEFAULT 0 COMMENT 'Tiempo en segundos de solape entre un clip y otro',
  `InfoProgram_PID` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `InfoProgram_Name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `InfoProgram_Code` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Hace referencia a tb_info_workflow_channel',
  PRIMARY KEY (`CapturePlan_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_capture_plan_span`
--

DROP TABLE IF EXISTS `tb_capture_plan_span`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_capture_plan_span` (
  `Span_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CapturePlan_ID` int(11) NOT NULL DEFAULT 0,
  `TiempoIni` time DEFAULT NULL,
  `TiempoFin` time DEFAULT NULL,
  `DiasEjecucion` tinyint(4) DEFAULT NULL COMMENT 'Guardamos los días de la semana en los distintos bits, empezando por Domingo en 1 << 0, Lunes en 1 << 1, etc',
  PRIMARY KEY (`Span_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_capture_task`
--

DROP TABLE IF EXISTS `tb_capture_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_capture_task` (
  `CaptureTask_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Server_ID` int(11) NOT NULL DEFAULT 0,
  `TiempoIni` datetime NOT NULL DEFAULT current_timestamp(),
  `TiempoFin` datetime NOT NULL DEFAULT current_timestamp(),
  `MuxID` int(11) NOT NULL DEFAULT -1,
  `InputVideo_ID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CurrentState` enum('unknown','waiting','running','completed','cancelled','error','cleaning_completed','cleaning_error') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'unknown',
  `ErrorMessage` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bCheckingStreamings` tinyint(4) NOT NULL DEFAULT 0,
  `bAllStreamingsAreReady` tinyint(4) NOT NULL DEFAULT 0,
  `LastTimeRecordingTaskFound` datetime NOT NULL DEFAULT current_timestamp(),
  `StreamingPortDict` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`CaptureTask_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_channel`
--

DROP TABLE IF EXISTS `tb_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_channel` (
  `Canal_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ChannelID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Canal_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=4096;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_channel_broadcast`
--

DROP TABLE IF EXISTS `tb_channel_broadcast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_channel_broadcast` (
  `Channel_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `Title` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Company` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `VideoDB_Name` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `VideoDB_Index` int(11) DEFAULT 0,
  `VideoDB_ServerURL` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `VideoDB_ServerUploadEntry` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bRadio` tinyint(4) NOT NULL DEFAULT 0,
  `URL_Thumbnail` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Channel_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=230;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_channel_tdt`
--

DROP TABLE IF EXISTS `tb_channel_tdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_channel_tdt` (
  `Channel_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `Title` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Company` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PID` int(11) DEFAULT 0,
  `bRadio` tinyint(4) NOT NULL DEFAULT 0,
  `MuxID` int(11) DEFAULT NULL,
  `Frequency` int(11) DEFAULT NULL,
  `Resolution` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Thumbnail` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `MainAudioTrackID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Channel_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=830 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_clip_export`
--

DROP TABLE IF EXISTS `tb_clip_export`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_clip_export` (
  `ClipExport_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ClipInfo_ID` int(11) NOT NULL,
  `Usuario_ID` int(11) NOT NULL COMMENT 'Usuario que genera el clip',
  `FeedInfo_ID` int(11) NOT NULL,
  `DispositivoInput_ID` int(11) DEFAULT NULL,
  `Task_ID` int(11) DEFAULT NULL,
  `Workflow_ID` int(11) DEFAULT NULL,
  `Estado` enum('pendiente','procesando','finalizado','error') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'pendiente',
  `Progreso` float(11,0) NOT NULL DEFAULT 0 COMMENT 'Progreso en porcentaje (0-100)',
  `FechaAlta` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Fecha de creación',
  `FechaEdited` datetime DEFAULT NULL COMMENT 'Fecha de ultima modificación',
  `FechaGenerated` datetime DEFAULT NULL,
  `FechaIni` datetime DEFAULT NULL COMMENT 'Inicio de edición',
  `FechaFin` datetime DEFAULT NULL COMMENT 'Final del edición',
  `Duracion` int(11) DEFAULT NULL COMMENT 'Duracion del resumen',
  `Tamano` float(11,0) DEFAULT NULL COMMENT 'Tamano en bytes del resumen',
  `Fragmentos` int(11) DEFAULT NULL COMMENT 'Numero de fragmentos que componen el resumen',
  `FragmentosData` text COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'JSON con info de fragmentos (orden, tiempos de inicio y fin) de fragmentos que componen el resumen. Ej. {orden: 1, msIni: 150332, msFin: 582732}',
  `Titulo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PerfilCodificacion_ID` int(11) DEFAULT NULL,
  `TipoUbicacion` int(11) NOT NULL DEFAULT 1 COMMENT '0 -> Ninguna, 1 -> Servidor, 2 -> FTP, 3 -> S3, 4 -> Dropbox',
  `FtpExportacion_ID` int(11) DEFAULT NULL,
  `URL_Exportacion` varchar(1000) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'URL de acceso al clip de salida',
  `URL_Thumbnail` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Thumbnail representativo del clip',
  `URL_Video` varchar(1000) CHARACTER SET utf8 DEFAULT '' COMMENT 'URL_Exportacion + Titulo + .mp4',
  `InfoProgram_Name` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PATH_XML` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'file:/data/rtve/home/import/data/zz_fichero_mp4_fhd',
  `FechaVisited` datetime DEFAULT NULL,
  `Resolucion` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Si está vacio, indica que no se ha obtenido y no se podrá generar los dummies',
  `Transicion` enum('ninguna','fundido') COLLATE utf8_spanish_ci DEFAULT 'ninguna' COMMENT 'Tipo de transición a aplicar entre fragmentos',
  `bGenerated` tinyint(4) NOT NULL DEFAULT 0,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `bDVR` tinyint(4) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`ClipExport_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_clip_info`
--

DROP TABLE IF EXISTS `tb_clip_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_clip_info` (
  `ClipInfo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Usuario_ID` int(11) NOT NULL COMMENT 'Usuario que genera el clip',
  `FeedInfo_ID` int(11) NOT NULL,
  `DispositivoInput_ID` int(11) DEFAULT NULL,
  `Task_ID` int(11) DEFAULT NULL,
  `Workflow_ID` int(11) DEFAULT NULL,
  `Estado` enum('pendiente','procesando','finalizado','error') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'pendiente',
  `Progreso` float(11,0) NOT NULL DEFAULT 0 COMMENT 'Progreso en porcentaje (0-100)',
  `FechaAlta` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Fecha de creación',
  `FechaEdited` datetime DEFAULT NULL COMMENT 'Fecha de ultima modificación',
  `FechaGenerated` datetime DEFAULT NULL,
  `FechaIni` datetime DEFAULT NULL COMMENT 'Inicio de edición',
  `FechaFin` datetime DEFAULT NULL COMMENT 'Final del edición',
  `Duracion` int(11) DEFAULT NULL COMMENT 'Duracion del resumen',
  `Tamano` float(11,0) DEFAULT NULL COMMENT 'Tamano en bytes del resumen',
  `Fragmentos` int(11) DEFAULT NULL COMMENT 'Numero de fragmentos que componen el resumen',
  `FragmentosData` text COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'JSON con info de fragmentos (orden, tiempos de inicio y fin) de fragmentos que componen el resumen. Ej. {orden: 1, msIni: 150332, msFin: 582732}',
  `Titulo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PerfilCodificacion_ID` int(11) DEFAULT NULL,
  `TipoUbicacion` int(11) NOT NULL DEFAULT 1 COMMENT '0 -> Ninguna, 1 -> Servidor, 2 -> FTP, 3 -> S3, 4 -> Dropbox',
  `FtpExportacion_ID` int(11) DEFAULT NULL,
  `URL_Exportacion` varchar(1000) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'URL de acceso al clip de salida',
  `URL_Thumbnail` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Thumbnail representativo del resumen',
  `URL_Video` varchar(1000) CHARACTER SET utf8 DEFAULT '' COMMENT 'URL del feed de origen',
  `FechaVisited` datetime DEFAULT NULL,
  `Resolucion` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Si está vacio, indica que no se ha obtenido y no se podrá generar los dummies',
  `Transicion` enum('ninguna','fundido') COLLATE utf8_spanish_ci DEFAULT 'ninguna' COMMENT 'Tipo de transición a aplicar entre fragmentos',
  `bGenerated` tinyint(4) NOT NULL DEFAULT 0,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `bDVR` tinyint(4) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`ClipInfo_ID`,`Usuario_ID`,`FeedInfo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_codificacion_data`
--

DROP TABLE IF EXISTS `tb_codificacion_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_codificacion_data` (
  `RecordingCodificationData_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CapturePlan_ID` int(11) unsigned NOT NULL COMMENT 'ID del CapturePlan al que pertenece',
  `PerfilCodificacion_ID` int(11) unsigned NOT NULL COMMENT 'ID de la tabla tb_perfil_codificacion',
  `AudioMapping` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `WatermarkPath` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `WatermarkPosition` int(11) DEFAULT NULL,
  `InternalStreamingURL` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `InternalStreamingPort` int(11) DEFAULT NULL,
  `OutputPath` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Path de salida, en caso de ser una codificación usada para generar un fichero',
  `PlanStreaming_ID` int(11) DEFAULT NULL COMMENT 'ID de la tabla tb_plan_streaming con el plan de streaming al que pertenece esta salida, en caso de tener streaming',
  `PlanAnalisis_ID` int(11) DEFAULT NULL COMMENT 'ID de la tabla tb_perfil_analisis con el plan de análisis en directo al que pertenece esta salida',
  PRIMARY KEY (`RecordingCodificationData_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_coding_profile`
--

DROP TABLE IF EXISTS `tb_coding_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_coding_profile` (
  `CodingProfile_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CodingProfileID` varchar(255) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'Campo ID del XML',
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT '',
  `VideoCodec_ID` int(11) NOT NULL DEFAULT 2,
  `AudioCodec_ID` int(11) NOT NULL DEFAULT 1,
  `bResetTimestamps` tinyint(4) NOT NULL DEFAULT 1,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`CodingProfile_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=16384;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_color_sampling`
--

DROP TABLE IF EXISTS `tb_color_sampling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_color_sampling` (
  `ColorSampling_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ColorSamplingID` varchar(255) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'Campo Name del XML',
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'YUV420',
  `Libreria` varchar(255) COLLATE utf8_spanish_ci DEFAULT '9',
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`ColorSampling_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=5461;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_destino_tipo`
--

DROP TABLE IF EXISTS `tb_destino_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_destino_tipo` (
  `DestinoTipo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `TargetTypeID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Modo` enum('captura','fichero') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'captura',
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`DestinoTipo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=4096;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_device`
--

DROP TABLE IF EXISTS `tb_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_device` (
  `Dispositivo_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DeviceID` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `GlobalID` int(11) DEFAULT NULL,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TipoFuente_ID` int(11) DEFAULT NULL,
  `DeviceType` enum('Unknown','SDICard','IPCamera','TDTCard','AudioDevice','HDMIDevice') COLLATE utf8_spanish_ci DEFAULT 'Unknown',
  `IP_WS` varchar(250) COLLATE utf8_spanish_ci DEFAULT '',
  `Grupo_ID` int(11) DEFAULT 0,
  `GroupID` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `LastImageSent` int(11) DEFAULT NULL,
  `Angle` decimal(10,2) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Dispositivo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1170;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_device_input`
--

DROP TABLE IF EXISTS `tb_device_input`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_device_input` (
  `DispositivoInput_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `InputID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `InputID_Video` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `InputID_Audio` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `InputType` enum('image','video','tdt_deco') COLLATE utf8_spanish_ci DEFAULT 'video',
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TipoFuente_ID` int(11) NOT NULL,
  `DeviceType` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `Dispositivo_ID` int(11) DEFAULT NULL,
  `DeviceID` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `Grupo_ID` int(11) DEFAULT NULL COMMENT 'Canal_ID: Por retrocompatibilidad capturer NOATUM',
  `GroupID` varchar(255) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'ChannelID: Por retrocompatibilidad capturer NOATUM',
  `Resolution` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FPS` int(11) DEFAULT 25,
  `Delay` int(11) DEFAULT 0,
  `ColorSampling_ID` int(255) DEFAULT NULL,
  `ColorSamplingID` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `URL` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Code` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Mux` int(11) DEFAULT NULL,
  `Frequency` int(11) DEFAULT NULL,
  `AudioChannels` enum('stereo','mono','n_16') COLLATE utf8_spanish_ci DEFAULT 'stereo',
  `Entrelazado` enum('progresivo','entrelazado') COLLATE utf8_spanish_ci DEFAULT 'progresivo',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `Channel_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DispositivoInput_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1015 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=496;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_emision_streaming`
--

DROP TABLE IF EXISTS `tb_emision_streaming`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_emision_streaming` (
  `EmisionStreaming_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `EmisionPlataforma` enum('url','wowza','youtube','facebook','instagram','periscope','twitter') COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Emision` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Key_Emision` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `User_Emision` varchar(100) COLLATE utf8_spanish_ci DEFAULT '',
  `Pass_Emision` varchar(100) COLLATE utf8_spanish_ci DEFAULT '',
  `DuracionSegundos` int(11) NOT NULL DEFAULT 0,
  `PerfilCodificacion_ID` int(11) DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`EmisionStreaming_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=3276;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_feed_info`
--

DROP TABLE IF EXISTS `tb_feed_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_feed_info` (
  `FeedInfo_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DispositivoInput_ID` int(11) DEFAULT NULL,
  `Task_ID` int(11) DEFAULT NULL,
  `Workflow_ID` int(11) DEFAULT NULL,
  `Titulo` varchar(1000) DEFAULT NULL COMMENT 'Nombre de la entrada del dispositivo (si DispositivoInput_ID > 0) o Titulo de la tarea (si Task_ID > 0) que ha creado el feed',
  `URL_Thumbnail` varchar(1000) DEFAULT NULL,
  `URL_Video` varchar(1000) DEFAULT NULL,
  `FPS` int(11) NOT NULL DEFAULT 25,
  `Bitrate` int(11) NOT NULL DEFAULT 0 COMMENT 'Bitrate de la entrada del feed. Si 0 indica un feed multibitrate',
  `Resolution` varchar(10) DEFAULT NULL COMMENT 'Resolucion de la entrada del feed. Si vacio indica un feed multibitrate',
  `Duration` decimal(10,0) DEFAULT NULL COMMENT 'Duración especificado en WF para la tarea creadora del feed',
  `StartTime` datetime DEFAULT NULL,
  `EndTime` datetime DEFAULT NULL,
  `Tipo` enum('hls','hls-multi','dash','dash-multi','mp4','hls-dash') DEFAULT 'hls',
  `Estado` enum('nofeed','recording','finished','error') DEFAULT 'nofeed',
  `bActivo` tinyint(4) unsigned NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) unsigned NOT NULL DEFAULT 1,
  `bDVR` tinyint(4) unsigned NOT NULL DEFAULT 0,
  `RecordingTask_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`FeedInfo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2859 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_ffmpeg_param`
--

DROP TABLE IF EXISTS `tb_ffmpeg_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_ffmpeg_param` (
  `FfmpegParam_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci NOT NULL DEFAULT '' COMMENT 'Nombre del parámetro a modificar',
  `Codigo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '' COMMENT 'String con el parámetro que hay que pasarle a ffmpeg',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`FfmpegParam_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_folder_config`
--

DROP TABLE IF EXISTS `tb_folder_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_folder_config` (
  `FolderConfig_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_RemotePath` varchar(1000) COLLATE utf8_spanish_ci NOT NULL,
  `TipoProceso_ID` enum('video','audio','album','photo') COLLATE utf8_spanish_ci DEFAULT 'video',
  `bSubdirectorios` tinyint(4) NOT NULL DEFAULT 0,
  `TestigoExtension` enum('sin-testigo','.nfo','.xml','.txt') COLLATE utf8_spanish_ci DEFAULT 'sin-testigo' COMMENT 'Vacio: NO hay testigo. Informado: SI extension del fichero testigo (.txt .nfo .xml)',
  `ModoEjecucion` enum('unica','encuesta','evento','carpeta') COLLATE utf8_spanish_ci DEFAULT 'unica',
  `EncuestaSegundos` int(11) NOT NULL DEFAULT 0 COMMENT 'Minimo 30 seg',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`FolderConfig_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_folder_process`
--

DROP TABLE IF EXISTS `tb_folder_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_folder_process` (
  `FolderProcess_ID` int(11) NOT NULL,
  `FolderConfig_ID` int(11) DEFAULT NULL COMMENT 'Indica la configuracion que da origen al procesamiento',
  `FilePath` varchar(2000) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `FileName` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `FechaAlta` datetime NOT NULL DEFAULT current_timestamp(),
  `FechaActualizacion` datetime DEFAULT NULL,
  `FechaProceso` datetime DEFAULT NULL,
  `bProcesado` tinyint(4) NOT NULL DEFAULT 0,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`FolderProcess_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_fps`
--

DROP TABLE IF EXISTS `tb_fps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_fps` (
  `FPS_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FpsID` varchar(255) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'Campo Name del XML',
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Valor` decimal(10,2) NOT NULL DEFAULT 25.00,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`FPS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=5461;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_fuente_tipo`
--

DROP TABLE IF EXISTS `tb_fuente_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_fuente_tipo` (
  `FuenteTipo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SourceTypeID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `Modo` enum('captura','fichero') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'captura',
  `FileType` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'video|audio' COMMENT 'video|audio|foto',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`FuenteTipo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1260;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_gpu`
--

DROP TABLE IF EXISTS `tb_gpu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_gpu` (
  `Gpu_ID` int(11) NOT NULL,
  `HwaccelDevice_ID` int(11) DEFAULT NULL,
  `NumberOfTranscodingSlots` int(11) DEFAULT NULL,
  `NumberOfLiveTranscodingSlots` int(11) DEFAULT NULL,
  `AvailableTranscodingSlots` int(11) DEFAULT NULL,
  `AvailableLiveTranscodingSlots` int(11) DEFAULT NULL,
  `Server_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Gpu_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_group`
--

DROP TABLE IF EXISTS `tb_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_group` (
  `Grupo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `GroupID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Grupo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1638;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_info_ftp`
--

DROP TABLE IF EXISTS `tb_info_ftp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_info_ftp` (
  `Ftp_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FtpID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL` varchar(500) COLLATE utf8_spanish_ci DEFAULT '',
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `FtpTranfer_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'Referencia al ID de tb_info_ftp_transfer que utilizara',
  `ModoUso` enum('fuente','destino') COLLATE utf8_spanish_ci NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Ftp_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=197 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1820;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_info_ftp_transfer`
--

DROP TABLE IF EXISTS `tb_info_ftp_transfer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_info_ftp_transfer` (
  `Ftp_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `Host` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'IP o dominio',
  `Port` int(11) DEFAULT 21,
  `UserName` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `UserPass` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `RemotePath` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `LocalPath` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SizeBlock` double DEFAULT 1000,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Ftp_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1820;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_info_live_server`
--

DROP TABLE IF EXISTS `tb_info_live_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_info_live_server` (
  `LiveServer_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`LiveServer_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=16384;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_info_portal_deploy`
--

DROP TABLE IF EXISTS `tb_info_portal_deploy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_info_portal_deploy` (
  `Portal_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PortalID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PortalName` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `SendVideo` tinyint(4) NOT NULL DEFAULT 0,
  `URL_Deploy` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'http://cms.etiqmediasites.com/...',
  `Credenciales` text COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'A futuro: AÃ±adir todos los campos necesarios para todas las app auxiliares',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Portal_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=8192;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_info_red_social`
--

DROP TABLE IF EXISTS `tb_info_red_social`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_info_red_social` (
  `RedSocial_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `RedSocialID` enum('YouTube','FaceBook','Twitter','Twitch','Dailymotion','LaLigaSports','Livestream','Instagram') COLLATE utf8_spanish_ci DEFAULT 'YouTube',
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `URL_Fuente` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Destino` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Credenciales` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `ModoUso` enum('fuente','destino') COLLATE utf8_spanish_ci DEFAULT NULL,
  `Channel_ID` int(11) DEFAULT NULL COMMENT 'Al usar como fuente se cogen los datos de tb_info_youtube_channel',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`RedSocial_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=4096;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_info_web`
--

DROP TABLE IF EXISTS `tb_info_web`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_info_web` (
  `Web_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `WebID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `URL_Fuente` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Destino` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Credenciales` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `LiveServer_ID` int(11) DEFAULT NULL COMMENT 'Al usar como fuente se cogen los datos de tb_info_live_server',
  `Portal_ID` int(11) DEFAULT NULL COMMENT 'Al usar como destino se cogen los datos de tb_info_portal_deploy',
  `ModoUso` enum('fuente','destino') COLLATE utf8_spanish_ci NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Web_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_info_workflow_channel`
--

DROP TABLE IF EXISTS `tb_info_workflow_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_info_workflow_channel` (
  `Channel_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Program` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PID` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Title` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CapturePlan_ID` int(11) DEFAULT NULL,
  `URL_Thumbnail` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL,
  PRIMARY KEY (`Channel_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=2730;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_info_youtube_channel`
--

DROP TABLE IF EXISTS `tb_info_youtube_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_info_youtube_channel` (
  `Channel_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Usuario` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `YT_Title` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `YT_Channel` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `YT_Url` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `YT_Description` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `YT_PrivacyDefault` enum('public','private','unlisted') COLLATE utf8_spanish_ci DEFAULT 'private',
  `Credenciales` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Channel_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=5461;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_muxer`
--

DROP TABLE IF EXISTS `tb_muxer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_muxer` (
  `Muxer_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MuxerID` varchar(255) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'Campo Name del XML',
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'MP4',
  `Extension` varchar(255) COLLATE utf8_spanish_ci DEFAULT '.mp4',
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Muxer_ID`),
  KEY `Muxer_ID` (`Muxer_ID`,`Titulo`),
  KEY `Titulo` (`Titulo`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=2340;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_online_subtitles_info`
--

DROP TABLE IF EXISTS `tb_online_subtitles_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_online_subtitles_info` (
  `OnlineSubtitlesInfo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bUsarPuntuador` tinyint(4) NOT NULL DEFAULT 0,
  `OnlineSubtitlesYoutube_ID` int(11) NOT NULL DEFAULT 0,
  `OnlineSubtitlesWowza_ID` int(11) NOT NULL DEFAULT 0,
  `OnlineSubtitlesSrt_ID` int(11) NOT NULL DEFAULT 0,
  `OnlineSubtitlesNewfor_ID` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`OnlineSubtitlesInfo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_online_subtitles_newfor`
--

DROP TABLE IF EXISTS `tb_online_subtitles_newfor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_online_subtitles_newfor` (
  `OnlineSubtitlesNewfor_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IP` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `Puerto` int(11) unsigned NOT NULL,
  `LineaInicio` int(11) unsigned NOT NULL COMMENT 'Línea del teletexto en la que se colocará la primera línea.',
  `bGuardarSRT` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`OnlineSubtitlesNewfor_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_online_subtitles_srt`
--

DROP TABLE IF EXISTS `tb_online_subtitles_srt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_online_subtitles_srt` (
  `OnlineSubtitlesSrt_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `OutputFilePath` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`OnlineSubtitlesSrt_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_online_subtitles_wowza`
--

DROP TABLE IF EXISTS `tb_online_subtitles_wowza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_online_subtitles_wowza` (
  `OnlineSubtitlesWowza_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `WowzaLink` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `Application` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `StreamName` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `WowzaPort` int(11) NOT NULL DEFAULT 8088,
  PRIMARY KEY (`OnlineSubtitlesWowza_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_online_subtitles_youtube`
--

DROP TABLE IF EXISTS `tb_online_subtitles_youtube`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_online_subtitles_youtube` (
  `OnlineSubtitlesYoutube_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SubtitlesApiKey` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`OnlineSubtitlesYoutube_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_origen`
--

DROP TABLE IF EXISTS `tb_origen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_origen` (
  `Origen_ID` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Representa ChannelID + [ GroupID | DeviceID | InputID ]',
  `TipoFuente_ID` int(11) NOT NULL DEFAULT 1 COMMENT 'IP: ChannelID + [ DeviceID | GroupID ]',
  `DeviceType` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'SDI | IP | TDT | Audio | RedSocial',
  `DeviceID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Dispositivo_ID` int(11) DEFAULT NULL,
  `InputType` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `InputID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `DispositivoInput_ID` int(11) DEFAULT NULL,
  `GroupID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Grupo_ID` int(11) DEFAULT NULL,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_IP` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_WS_ConfigDevice_WP` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'URL donde se envia el working plan para cada uno de los origenes (SDI, YouTube, etc)',
  `bUsado` tinyint(4) DEFAULT 0,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Origen_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=16384;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_perfil_algoritmo`
--

DROP TABLE IF EXISTS `tb_perfil_algoritmo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_perfil_algoritmo` (
  `PerfilAlgoritmo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Tipo_ID` enum('objetos','caras','subtitulado-directo') COLLATE utf8_spanish_ci DEFAULT NULL,
  `InfoExtra` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Información extra de los diferentes algoritmos. Ej: en subtitulado en directo, idiomas que subtitula separados por pipes',
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PerfilAlgoritmo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1638;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_perfil_analisis`
--

DROP TABLE IF EXISTS `tb_perfil_analisis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_perfil_analisis` (
  `PerfilAnalisis_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `-Tipo_ID` enum('objetos','caras','subtitulado-directo') COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PerfilAnalisis_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=4096;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_perfil_analisis_algoritmo`
--

DROP TABLE IF EXISTS `tb_perfil_analisis_algoritmo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_perfil_analisis_algoritmo` (
  `PerfilAnalisis_ID` int(11) NOT NULL,
  `Tipo_ID` enum('objetos','caras','subtitulado-directo') COLLATE utf8_spanish_ci NOT NULL,
  `PerfilAlgoritmo_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PerfilAnalisis_ID`,`Tipo_ID`,`PerfilAlgoritmo_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1365;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_perfil_codificacion`
--

DROP TABLE IF EXISTS `tb_perfil_codificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_perfil_codificacion` (
  `PerfilCodificacion_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `VideoOrigin` enum('Fichero','Capturadora') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'Fichero' COMMENT 'Fichero|Capturadora',
  `Contenedor` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Muxer_ID` int(11) DEFAULT NULL,
  `MuxerID` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `Width` int(11) DEFAULT NULL,
  `Height` int(11) DEFAULT NULL,
  `Resolution` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `AspectRatio_ID` int(11) DEFAULT NULL,
  `AspectRatioID` enum('16:9','4:3') COLLATE utf8_spanish_ci DEFAULT NULL,
  `ColorSampling_ID` int(11) NOT NULL,
  `ColorSamplingID` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `FpsID` enum('25','50','15') COLLATE utf8_spanish_ci NOT NULL DEFAULT '25',
  `VideoCodec_ID` int(11) DEFAULT NULL,
  `VideoCodecID` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `VideoBitrate` int(11) DEFAULT 0,
  `VideoBitrateMax` int(11) DEFAULT NULL,
  `VideoCodecProfile` enum('low','main','high','baseline') COLLATE utf8_spanish_ci DEFAULT NULL,
  `VideoCodecLevel` enum('3_0','3_1','3_2','4_0','4_1') COLLATE utf8_spanish_ci DEFAULT NULL,
  `BufSize` int(11) DEFAULT NULL,
  `GopSize` int(11) DEFAULT NULL,
  `AudioCodec_ID` int(11) DEFAULT NULL,
  `AudioCodecID` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `AudioBitrate` int(11) DEFAULT 25,
  `AudioSampleRate` int(11) DEFAULT NULL,
  `AudioCodecProfile` enum('none','aac_he','aac_he_v2') COLLATE utf8_spanish_ci DEFAULT 'none',
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bPermiteStreaming` tinyint(4) NOT NULL DEFAULT 0,
  `bPermiteCaptura` tinyint(4) NOT NULL DEFAULT 0,
  `bPermiteHLS` tinyint(4) NOT NULL DEFAULT 0,
  `bPermiteDASH` tinyint(4) NOT NULL DEFAULT 0,
  `bStrictExperimental` tinyint(4) NOT NULL DEFAULT 0,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PerfilCodificacion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=963;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_perfil_codificacion_backup`
--

DROP TABLE IF EXISTS `tb_perfil_codificacion_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_perfil_codificacion_backup` (
  `PerfilCodificacion_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `VideoOrigin` enum('Fichero','Capturadora') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'Fichero' COMMENT 'Fichero|Capturadora',
  `Contenedor` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `Muxer_ID` int(11) DEFAULT NULL,
  `MuxerID` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `Width` int(11) DEFAULT NULL,
  `Height` int(11) DEFAULT NULL,
  `Resolution` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `AspectRatioID` enum('16:9','4:3') COLLATE utf8_spanish_ci DEFAULT NULL,
  `ColorSampling_ID` int(255) NOT NULL,
  `ColorSamplingID` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `FpsID` enum('25','50','15') COLLATE utf8_spanish_ci NOT NULL DEFAULT '25',
  `VideoCodec_ID` int(11) DEFAULT NULL,
  `VideoCodecID` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `VideoBitrate` int(11) DEFAULT 0,
  `AudioCodec_ID` int(11) DEFAULT NULL,
  `AudioCodecID` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `AudioBitrate` int(11) DEFAULT 25,
  `AudioSampleRate` int(11) DEFAULT NULL,
  `bPermiteStreaming` tinyint(4) NOT NULL DEFAULT 0,
  `bPermiteCaptura` tinyint(4) NOT NULL DEFAULT 0,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PerfilCodificacion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1365;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_perfil_codificacion_ffmpeg`
--

DROP TABLE IF EXISTS `tb_perfil_codificacion_ffmpeg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_perfil_codificacion_ffmpeg` (
  `PerfilCodificacionFfmpeg_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PerfilCodificacion_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'Perfil de Codificación al que pertenece',
  `FfmpegParam_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'Parámetro de ffmpeg',
  `Valor` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '' COMMENT 'Valor que se le tiene que dal al parámetro',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PerfilCodificacionFfmpeg_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_perfil_exportacion`
--

DROP TABLE IF EXISTS `tb_perfil_exportacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_perfil_exportacion` (
  `PerfilExportacion_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TipoExportacion` enum('ninguno','fichero','playlist') COLLATE utf8_spanish_ci DEFAULT 'fichero',
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PerfilExportacion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_perfil_exportacion_indexacion`
--

DROP TABLE IF EXISTS `tb_perfil_exportacion_indexacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_perfil_exportacion_indexacion` (
  `PerfilExportacion_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TipoExportacion` enum('indexacion-transcoding-info-xml','indexacion-transcripcion-doc','indexacion-transcripcion-txt','indexacion-transcripcion-pdf','indexacion-subtitulo-srt','indexacion-subtitulo-stl','indexacion-subtitulo-vtt','indexacion-metadatos-xml','indexacion-metadatos-json','indexacion-oradores-vtt','indexacion-hash-fichero','indexacion-video-fichero','indexacion-metadatos-rtve','indexacion-metadatos-atresmedia','indexacion-error-fichero') COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PerfilExportacion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_perfil_exportacion_transcodificacion`
--

DROP TABLE IF EXISTS `tb_perfil_exportacion_transcodificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_perfil_exportacion_transcodificacion` (
  `PerfilExportacion_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TipoExportacion` enum('transcodificacion-fichero','transcodificacion-playlist','transcodificacion-preroll') COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PerfilExportacion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_perfil_indexacion`
--

DROP TABLE IF EXISTS `tb_perfil_indexacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_perfil_indexacion` (
  `PerfilIndexacion_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FileType` enum('Audio','Video','Foto') COLLATE utf8_spanish_ci DEFAULT NULL,
  `CalculateHash` tinyint(4) NOT NULL DEFAULT 0,
  `Channel_ID` int(11) DEFAULT NULL COMMENT 'tb_channel_broadcast',
  `BroadcastChannelID` varchar(200) COLLATE utf8_spanish_ci DEFAULT '',
  `ContentTypeID` enum('Politica_varios_hablantes','Politica_1_hablante','Informativo','Informativo RNE','Informativo RTVE','Informativo A3','Informativo LaSexta','CorteOriginal','Futbol','Institucional','Video_Institucional_10_30','Audio_Institucional_1_10','Audio_Institucional_10_30','Minutado_sincro_subtitulos','Minutado_VAD','CorteInfoMicros','Minutado_audio_varios_hablantes_1_20','CortesAragon','Divulgativo','Divulgativo_EN','Tenis','Baloncesto','Meteorologico','Formativo','Minutado','Minutado_varios_hablantes_1_10','Minutado_varios_hablantes_1_20','Video_varios_hablantes_2_10','Video_1_hablante','Video_varios_hablantes_1_10','Video_varios_hablantes_1_2','Video_varios_hablantes_2_20','Video_varios_hablantes_10_20','Video_varios_hablantes_2_30','Video_varios_hablantes_20_50','Audio_un_hablante','Audio_varios_hablantes_5_10','Audio_varios_hablantes_10_20','Audio_varios_hablantes_1_10','Audio_varios_hablantes_2_20','Audio_varios_hablantes_20_50','Senado','MinutadoAudio','MinutadoAudioInstitucional','Sin_procesar') COLLATE utf8_spanish_ci DEFAULT NULL,
  `StreamingServerID` enum('Servidor Local','Youtube','Vimeo') COLLATE utf8_spanish_ci DEFAULT NULL,
  `VideoOriginID` enum('Fichero','Capturadora') COLLATE utf8_spanish_ci DEFAULT NULL,
  `GeneralFaceDatabaseID` enum('BBDD Caras general','Sin_caras','ETIQMEDIA') COLLATE utf8_spanish_ci DEFAULT 'BBDD Caras general',
  `OCRProfileID` enum('Sin OCR','Rotulos_informativos','Presentacion_con_transparencias') COLLATE utf8_spanish_ci DEFAULT 'Sin OCR',
  `LogoProfileID` enum('Sin logos','MotoGP','Objetos') COLLATE utf8_spanish_ci DEFAULT 'Sin logos',
  `CategorizationServerID` enum('Conceptualizador y categorizador','Conceptualizador','Categorizador','Solo Carga') COLLATE utf8_spanish_ci DEFAULT NULL,
  `OntologyID` enum('IPTC') COLLATE utf8_spanish_ci DEFAULT 'IPTC',
  `ASRDictionaryID` enum('Sin ASR','cat_esp_informativos','esp_cat_informativos','esp_cat_institucional','Congreso_langV4_AnetV6_rvb','GeneralV15','UNED_V1_150h','Parlament_V6_rvb','Congreso_langV4_interpol_UNED_V1','justicia_v2-2_40h_forw','Informativos_full_TVE_API_v3','12092018_noDots_Anet_v6_Lnet_Forw','Informativos_full_TVE_API','EuropeanParliament','DPZ','CentroMedico','Informativos','CortesAragon','CortesCLM','Diccionario general','congreso','congreso_extended','congreso_cyl_rescore','congreso_extended_rescore','congreso_CA_rescore','general_redes','congreso_ZGZ_rescore','congreso_V3_noDots_Anet_115h_Lnet_Forw','Congreso_langV5_AnetV6_rvb','Congreso_V6','cat_esp_institucional','Brazilian_v3','val_esp_instituciones_v2','eus_institucional_V1','bal_institucional_v1','val_institucional_V1','bale_esp_informativo_v1','cat_esp_tv3-v1-0_v1-0_v1-0','ba_es_inst_v1-0_v1-0_inst_v1-0_v1-0_inst_v1-0','val_esp_inst-v1-0_v1-0_v1-0','es_cong_v1_0_inst_v1_0','ba_esp_inst-v1-0_v1-0_v2-0','ba_esp_inst-v1-0_v1-0_v3-0','cat_esp_tv3-v2-0_v1-0_v1-1','ba_esp_inst-v1-0_v1-0_v3-0') COLLATE utf8_spanish_ci DEFAULT 'Sin ASR',
  `AudioSyncProfileID` enum('Sin sincronizador','Minutado','Texto','Minutado STL') COLLATE utf8_spanish_ci DEFAULT NULL,
  `ThesaurusID` enum('Sin tesauro','ETIQMEDIA','TV3','BASTEM','PROTEM') COLLATE utf8_spanish_ci DEFAULT NULL,
  `PerfilCodificacion_ID` int(11) DEFAULT NULL COMMENT 'tb_perfil_codificacion',
  `CodindgProfileID` varchar(200) COLLATE utf8_spanish_ci DEFAULT '',
  `KeepOriginalVideo` tinyint(4) NOT NULL DEFAULT 1,
  `LowResCopy` tinyint(4) NOT NULL DEFAULT 0,
  `Priority` enum('1','2','3','4','5') COLLATE utf8_spanish_ci DEFAULT '5',
  `SendToPortalBeforeProcess` enum('No','Subir','Subir con video') COLLATE utf8_spanish_ci DEFAULT 'No',
  `SendToPortalAfterProcess` enum('No','Subir','Subir con video') COLLATE utf8_spanish_ci DEFAULT 'No',
  `UploadToYouTubeChannel_ID` int(11) DEFAULT -1 COMMENT 'tb_info_youtube_channel',
  `DeployToPortal_ID` int(11) DEFAULT -1 COMMENT 'tb_info_portal_deploy',
  `UploadToFTP_ID` int(11) DEFAULT -1 COMMENT 'tb_info_ftp_transfer',
  `ExportFileTypesIDs` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'puede contener multiples valores: TXT|DOC|SRT|STL|VTT|PDF',
  `EmailNotificationsList` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Lista de grupos o emails separados por ''|'' ETIQMEDIA|CLIENTES o dmartinez@etiqmedia.com|aarguedas@etiqmedia.com',
  `ApiUploadProfileID` enum('') COLLATE utf8_spanish_ci DEFAULT NULL,
  `SkipFileCopy` tinyint(4) NOT NULL DEFAULT 0,
  `VisualizationRights` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Lista de grupos separados por ''|'' Editores|OperaciÃ³n',
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PerfilIndexacion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=712 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=364;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_perfil_indexacion_20201203`
--

DROP TABLE IF EXISTS `tb_perfil_indexacion_20201203`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_perfil_indexacion_20201203` (
  `PerfilIndexacion_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FileType` enum('Audio','Video','Foto') COLLATE utf8_spanish_ci DEFAULT NULL,
  `CalculateHash` tinyint(4) NOT NULL DEFAULT 0,
  `Channel_ID` int(11) DEFAULT NULL COMMENT 'tb_channel_broadcast',
  `BroadcastChannelID` varchar(200) COLLATE utf8_spanish_ci DEFAULT '',
  `ContentTypeID` enum('Politica_varios_hablantes','Politica_1_hablante','Informativo','Informativo RNE','Informativo RTVE','Informativo A3','Informativo LaSexta','CorteOriginal','Futbol','Institucional','Video_Institucional_10_30','Audio_Institucional_1_10','Audio_Institucional_10_30','Minutado_sincro_subtitulos','Minutado_VAD','CorteInfoMicros','Minutado_audio_varios_hablantes_1_20','CortesAragon','Divulgativo','Divulgativo_EN','Tenis','Baloncesto','Meteorologico','Formativo','Minutado','Minutado_varios_hablantes_1_10','Minutado_varios_hablantes_1_20','Politica_1_hablante','Politica_varios_hablantes','Video_varios_hablantes_2_10','Video_1_hablante','Video_varios_hablantes_1_10','Video_varios_hablantes_2_20','Video_varios_hablantes_10_20','Video_varios_hablantes_2_30','Video_varios_hablantes_20_50','Audio_un_hablante','Audio_varios_hablantes_5_10','Audio_varios_hablantes_10_20','Audio_varios_hablantes_1_10','Audio_varios_hablantes_2_20','Audio_varios_hablantes_20_50','Senado','MinutadoAudio','MinutadoAudioInstitucional','Sin_procesar') COLLATE utf8_spanish_ci DEFAULT NULL,
  `StreamingServerID` enum('Servidor Local','Youtube','Vimeo') COLLATE utf8_spanish_ci DEFAULT NULL,
  `VideoOriginID` enum('Fichero','Capturadora') COLLATE utf8_spanish_ci DEFAULT NULL,
  `GeneralFaceDatabaseID` enum('BBDD Caras general','Sin_caras','ETIQMEDIA') COLLATE utf8_spanish_ci DEFAULT 'BBDD Caras general',
  `OCRProfileID` enum('Sin OCR','Rotulos_informativos','Presentacion_con_transparencias') COLLATE utf8_spanish_ci DEFAULT 'Sin OCR',
  `LogoProfileID` enum('Sin logos','MotoGP') COLLATE utf8_spanish_ci DEFAULT 'Sin logos',
  `CategorizationServerID` enum('Conceptualizador y categorizador','Conceptualizador','Categorizador','Solo Carga') COLLATE utf8_spanish_ci DEFAULT NULL,
  `OntologyID` enum('IPTC') COLLATE utf8_spanish_ci DEFAULT 'IPTC',
  `ASRDictionaryID` enum('Sin ASR','cat_esp_informativos','esp_cat_informativos','esp_cat_institucional','Congreso_langV4_AnetV6_rvb','GeneralV15','UNED_V1_150h','Parlament_V6_rvb','Congreso_langV4_interpol_UNED_V1','justicia_v2-2_40h_forw','Informativos_full_TVE_API_v2','12092018_noDots_Anet_v6_Lnet_Forw','Informativos_full_TVE_API','EuropeanParliament','DPZ','CentroMedico','Informativos','CortesAragon','CortesCLM','Diccionario general','congreso','congreso_extended','congreso_cyl_rescore','congreso_extended_rescore','congreso_CA_rescore','general_redes','congreso_ZGZ_rescore','congreso_V3_noDots_Anet_115h_Lnet_Forw','Congreso_langV5_AnetV6_rvb','Congreso_V6','cat_esp_institucional') COLLATE utf8_spanish_ci DEFAULT 'Sin ASR',
  `AudioSyncProfileID` enum('Sin sincronizador','Minutado','Texto','Minutado STL') COLLATE utf8_spanish_ci DEFAULT NULL,
  `PerfilCodificacion_ID` int(11) DEFAULT NULL COMMENT 'tb_perfil_codificacion',
  `CodindgProfileID` varchar(200) COLLATE utf8_spanish_ci DEFAULT '',
  `KeepOriginalVideo` tinyint(4) NOT NULL DEFAULT 1,
  `LowResCopy` tinyint(4) NOT NULL DEFAULT 0,
  `Priority` enum('1','2','3','4','5') COLLATE utf8_spanish_ci DEFAULT '5',
  `SendToPortalBeforeProcess` enum('No','Subir','Subir con video') COLLATE utf8_spanish_ci DEFAULT 'No',
  `SendToPortalAfterProcess` enum('No','Subir','Subir con video') COLLATE utf8_spanish_ci DEFAULT 'No',
  `UploadToYouTubeChannel_ID` int(11) DEFAULT -1 COMMENT 'tb_info_youtube_channel',
  `DeployToPortal_ID` int(11) DEFAULT -1 COMMENT 'tb_info_portal_deploy',
  `UploadToFTP_ID` int(11) DEFAULT -1 COMMENT 'tb_info_ftp_transfer',
  `ExportFileTypesIDs` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'puede contener multiples valores: TXT|DOC|SRT|STL|VTT|PDF',
  `EmailNotificationsList` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Lista de grupos o emails separados por ''|'' ETIQMEDIA|CLIENTES o dmartinez@etiqmedia.com|aarguedas@etiqmedia.com',
  `ApiUploadProfileID` enum('') COLLATE utf8_spanish_ci DEFAULT NULL,
  `SkipFileCopy` tinyint(4) NOT NULL DEFAULT 0,
  `VisualizationRights` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Lista de grupos separados por ''|'' Editores|OperaciÃ³n',
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PerfilIndexacion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=678 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_perfil_indexacion_backup`
--

DROP TABLE IF EXISTS `tb_perfil_indexacion_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_perfil_indexacion_backup` (
  `PerfilIndexacion_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FileType` enum('Audio','Video','Foto') COLLATE utf8_spanish_ci DEFAULT NULL,
  `CalculateHash` tinyint(4) NOT NULL DEFAULT 0,
  `Channel_ID` int(11) DEFAULT NULL COMMENT 'tb_channel_broadcast',
  `BroadcastChannelID` varchar(200) COLLATE utf8_spanish_ci DEFAULT '',
  `ContentTypeID` enum('Informativo','Informativo RNE','Informativo RTVE','Informativo A3','Informativo LaSexta','CorteOriginal','Futbol','Institucional','Video_Institucional_10_30','Audio_Institucional_1_10','Audio_Institucional_10_30','Minutado_sincro_subtitulos','Minutado_VAD','CorteInfoMicros','Minutado_audio_varios_hablantes_1_20','CortesAragon','Divulgativo','Divulgativo_EN','Tenis','Baloncesto','Meteorologico','Formativo','Minutado','Minutado_varios_hablantes_1_10','Minutado_varios_hablantes_1_20','Politica_1_hablante','Politica_varios_hablantes','Video_varios_hablantes_2_10','Video_1_hablante','Video_varios_hablantes_1_10','Video_varios_hablantes_2_20','Video_varios_hablantes_10_20','Video_varios_hablantes_2_30','Video_varios_hablantes_20_50','Audio_un_hablante','Audio_varios_hablantes_5_10','Audio_varios_hablantes_10_20','Audio_varios_hablantes_1_10','Audio_varios_hablantes_2_20','Audio_varios_hablantes_20_50','Senado','MinutadoAudio','MinutadoAudioInstitucional') COLLATE utf8_spanish_ci DEFAULT NULL,
  `StreamingServerID` enum('Servidor Local','Youtube','Vimeo') COLLATE utf8_spanish_ci DEFAULT NULL,
  `VideoOriginID` enum('Fichero','Capturadora') COLLATE utf8_spanish_ci DEFAULT NULL,
  `GeneralFaceDatabaseID` enum('BBDD Caras general','Sin_caras','ETIQMEDIA') COLLATE utf8_spanish_ci DEFAULT 'BBDD Caras general',
  `OCRProfileID` enum('Sin OCR','Rotulos_informativos','Presentacion_con_transparencias') COLLATE utf8_spanish_ci DEFAULT 'Sin OCR',
  `LogoProfileID` enum('Sin logos','MotoGP') COLLATE utf8_spanish_ci DEFAULT 'Sin logos',
  `CategorizationServerID` enum('Conceptualizador y categorizador','Conceptualizador','Categorizador','Solo Carga') COLLATE utf8_spanish_ci DEFAULT NULL,
  `OntologyID` enum('IPTC') COLLATE utf8_spanish_ci DEFAULT 'IPTC',
  `ASRDictionaryID` enum('Sin ASR','cat_esp_informativos','esp_cat_informativos','esp_cat_institucional','Congreso_langV4_AnetV6_rvb','GeneralV15','UNED_V1_150h','Parlament_V6_rvb','Congreso_langV4_interpol_UNED_V1','justicia_v2-2_40h_forw','Informativos_full_TVE_API_v2','12092018_noDots_Anet_v6_Lnet_Forw','Informativos_full_TVE_API','EuropeanParliament','DPZ','CentroMedico','Informativos','CortesAragon','CortesCLM','Diccionario general','congreso','congreso_extended','congreso_cyl_rescore','congreso_extended_rescore','congreso_CA_rescore','general_redes','congreso_ZGZ_rescore','congreso_V3_noDots_Anet_115h_Lnet_Forw','Congreso_langV5_AnetV6_rvb','cat_esp_institucional') COLLATE utf8_spanish_ci DEFAULT 'Sin ASR',
  `AudioSyncProfileID` enum('Sin sincronizador','Minutado','Texto','Minutado STL') COLLATE utf8_spanish_ci DEFAULT NULL,
  `PerfilCodificacion_ID` int(11) DEFAULT NULL COMMENT 'tb_perfil_codificacion',
  `CodindgProfileID` varchar(200) COLLATE utf8_spanish_ci DEFAULT '',
  `KeepOriginalVideo` tinyint(4) NOT NULL DEFAULT 1,
  `LowResCopy` tinyint(4) NOT NULL DEFAULT 0,
  `Priority` enum('1','2','3','4','5') COLLATE utf8_spanish_ci DEFAULT '5',
  `SendToPortalBeforeProcess` enum('No','Subir','Subir con video') COLLATE utf8_spanish_ci DEFAULT 'No',
  `SendToPortalAfterProcess` enum('No','Subir','Subir con video') COLLATE utf8_spanish_ci DEFAULT 'No',
  `UploadToYouTubeChannel_ID` int(11) DEFAULT -1 COMMENT 'tb_info_youtube_channel',
  `DeployToPortal_ID` int(11) DEFAULT -1 COMMENT 'tb_info_portal_deploy',
  `UploadToFTP_ID` int(11) DEFAULT -1 COMMENT 'tb_info_ftp_transfer',
  `ExportFileTypesIDs` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'puede contener multiples valores: TXT|DOC|SRT|STL|VTT|PDF',
  `EmailNotificationsList` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Lista de grupos o emails separados por ''|'' ETIQMEDIA|CLIENTES o dmartinez@etiqmedia.com|aarguedas@etiqmedia.com',
  `ApiUploadProfileID` enum('') COLLATE utf8_spanish_ci DEFAULT NULL,
  `SkipFileCopy` tinyint(4) NOT NULL DEFAULT 0,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PerfilIndexacion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=539 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=3276;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_perfil_streaming`
--

DROP TABLE IF EXISTS `tb_perfil_streaming`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_perfil_streaming` (
  `PerfilStreaming_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PerfilStreamingID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `EmisionStreaming_ID` int(11) DEFAULT NULL,
  `PerfilCodificacion_ID` int(11) DEFAULT NULL,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`PerfilStreaming_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=5461;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_planificacion_preparado_capture_plan`
--

DROP TABLE IF EXISTS `tb_planificacion_preparado_capture_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_planificacion_preparado_capture_plan` (
  `Plan_ID` int(11) unsigned NOT NULL,
  `CapturePlan_ID` int(11) unsigned NOT NULL,
  PRIMARY KEY (`Plan_ID`,`CapturePlan_ID`),
  KEY `IDX_tb_planificacion_preparado_capture_plan_CapturePlan_ID` (`CapturePlan_ID`),
  KEY `IDX_tb_planificacion_preparado_capture_plan_Plan_ID` (`Plan_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_portal`
--

DROP TABLE IF EXISTS `tb_portal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_portal` (
  `Portal_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Tipo_ID` int(11) NOT NULL DEFAULT 400,
  `Tipo_W_A_N` enum('MANAGER','WORKFLOW','ANALYTICS','NAVIGATOR','ENDUSER','MEETINGS') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'MANAGER' COMMENT 'MANAGER | WORKFLOW | ANALYTICS | NAVIGATOR | ENDUSER | MEETINGS',
  `Code` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT 'RTVE',
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'Plataforma RTVE',
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Portal_Code` varchar(255) CHARACTER SET utf8 DEFAULT 'PORTAL_MANAGER_RTVE_ID',
  `Protocolo` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'http',
  `Dominio` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `URL_CDN` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'http://cdn.etiqmediasites.com/W-A-N/' COMMENT 'http://cdn-etiqmedia:8085/W-A-N/',
  `URL_Server_Data` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'http://em-platform-data:8083' COMMENT 'http://em-platform-data:8083',
  `Path_Server_TUS` varchar(500) CHARACTER SET utf8 DEFAULT '',
  `URL_Server_TUS` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT 'http://em-w-a-n-tus-local:8089',
  `Path_Server_Explorer` varchar(500) CHARACTER SET utf8 DEFAULT '',
  `URL_Server_Explorer` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `URL_Logo` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `FechaAlta` datetime NOT NULL DEFAULT current_timestamp(),
  `bPrivado` tinyint(4) NOT NULL DEFAULT 1,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `SOLR_Host` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `SOLR_Port` varchar(6) CHARACTER SET utf8 DEFAULT '',
  `SOLR_Core` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `SOLR_Path` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `SOLR_visualizationRights_Public` varchar(255) CHARACTER SET utf8 DEFAULT 'Público' COMMENT 'Permiso por DEFECTO que se asigna a todo el contenido de SOLR, necesario si necesitamos restringir contenidos mediante "visualizationRights: ..."',
  `URL_WS_Principal_Procesamiento` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT 'Portal MANAGER: URI del servidor principal de procesamiento',
  `URL_WS_Workflow_Envios` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT 'Portal WORKFLOW: URI donde se envÃ­an los workflows',
  `URL_WS_User` varchar(50) CHARACTER SET utf8 DEFAULT '' COMMENT 'Solo para URL_WS_Workflow_Envios',
  `URL_WS_Pass` varchar(50) CHARACTER SET utf8 DEFAULT '' COMMENT 'Solo para URL_WS_Workflow_Envios',
  `URL_WS_Analytics_Central` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MySQL_Host` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Info de conexion a BD MySQL desde los WS',
  `MySQL_Port` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Info de conexion a BD MySQL desde los WS',
  `MySQL_User` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Info de conexion a BD MySQL desde los WS',
  `MySQL_Pass` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Info de conexion a BD MySQL desde los WS',
  PRIMARY KEY (`Portal_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1420 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=3276;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_portal_parametro`
--

DROP TABLE IF EXISTS `tb_portal_parametro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_portal_parametro` (
  `Parametro_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Seccion_ID` int(10) unsigned DEFAULT NULL COMMENT 'Se corresponde con TipoPortal_ID',
  `TipoValor_ID` enum('booleano','numero','cadena','fecha') NOT NULL DEFAULT 'cadena',
  `Control_ID` enum('check','texto','texto-multiple','combo','combo-multiple','datepicker') NOT NULL DEFAULT 'texto',
  `Titulo` varchar(255) DEFAULT NULL,
  `CodigoAPP` varchar(100) DEFAULT NULL,
  `Valor` varchar(500) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Parametro_ID`),
  KEY `ParamTipo_ID` (`Seccion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10077 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=199;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_recording_codificacion_data`
--

DROP TABLE IF EXISTS `tb_recording_codificacion_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_recording_codificacion_data` (
  `RecordingCodificationData_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CapturePlan_ID` int(11) unsigned NOT NULL COMMENT 'ID del CapturePlan al que pertenece',
  `PerfilCodificacion_ID` int(11) unsigned NOT NULL COMMENT 'ID de la tabla tb_perfil_codificacion',
  `WatermarkPath` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `InternalStreamingURL` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `InternalStreamingPort` int(11) DEFAULT NULL,
  `OutputPath` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Path de salida, en caso de ser una codificación usada para generar un fichero',
  `PlanStreaming_ID` int(11) DEFAULT NULL COMMENT 'ID de la tabla tb_plan_streaming con el plan de streaming al que pertenece esta salida, en caso de tener streaming',
  `PlanAnalisis_ID` int(11) DEFAULT NULL COMMENT 'ID de la tabla tb_perfil_analisis con el plan de análisis en directo al que pertenece esta salida',
  `AudioMapping` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bSegmentAudio` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`RecordingCodificationData_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_recording_task`
--

DROP TABLE IF EXISTS `tb_recording_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_recording_task` (
  `RecordingTask_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CaptureTask_ID` int(11) NOT NULL DEFAULT 0,
  `Server_ID` int(11) DEFAULT NULL,
  `RecordingSuperTask_ID` int(11) NOT NULL DEFAULT -1,
  `StreamingSuperTask_ID` int(11) NOT NULL DEFAULT -1,
  `AnalysisSuperTask_ID` int(11) NOT NULL DEFAULT -1,
  `GPU_ID` int(11) NOT NULL DEFAULT -1,
  `InputURL` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TiempoIni` datetime NOT NULL DEFAULT current_timestamp(),
  `TiempoFin` datetime NOT NULL DEFAULT current_timestamp(),
  `ContentURL` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ContentThumbnailURL` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `OutputFolderPath` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `OutputFileName` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `OutputFileExtension` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `HLSPlayListFolderPath` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `HLSChunksFolderPath` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `DateFolder` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CapturePlan_ID` int(11) NOT NULL DEFAULT -1,
  `TipoCaptura` enum('ninguno','hls','ts','dash','hls-dash') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'ninguno',
  `Planificacion_ID` int(11) NOT NULL DEFAULT -1,
  `CurrentChunkNumber` int(11) NOT NULL DEFAULT -1,
  `LastFrame` int(11) NOT NULL DEFAULT 0,
  `LastChunkReleaseTime` timestamp NULL DEFAULT NULL,
  `CurrentState` enum('unknown','waiting','ready','starting','running','completed','error','cleaning_completed','cleaning_error') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'unknown',
  `ErrorMessage` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FeedInfoState` enum('no_feed','recording','finished','error') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'no_feed',
  `InternalProcessPID` int(11) NOT NULL DEFAULT -1,
  `bLaunchedBefore` tinyint(4) NOT NULL DEFAULT 0,
  `bFFMpegHasConnected` tinyint(4) NOT NULL DEFAULT 0,
  `TDTChannel_ID` int(11) unsigned DEFAULT NULL,
  `AudioMappings` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `NumberOfRetrys` int(11) unsigned NOT NULL DEFAULT 0,
  `ExportacionPeriodica_UltimaFecha` datetime NOT NULL DEFAULT current_timestamp(),
  `ExportacionPeriodica_Numero` int(11) NOT NULL DEFAULT 0,
  `ExportacionPeriodica_TiempoInicioProximoClip` int(11) NOT NULL DEFAULT 0,
  `bExportacionPeriodica_CorteProcesado` tinyint(4) NOT NULL DEFAULT 0,
  `currentPlaylistDuration` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `lastPlaylistDurationProcessed` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `timeWithoutInputStream` int(11) NOT NULL DEFAULT 0,
  `lastInputDataReceivedDate` datetime NOT NULL DEFAULT current_timestamp(),
  `bOnlyAudio` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`RecordingTask_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_status_info`
--

DROP TABLE IF EXISTS `tb_status_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_status_info` (
  `StatusInfo_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Tipo` enum('central','nodo','input') DEFAULT 'input',
  `Grupo_ID` int(11) DEFAULT NULL,
  `DispositivoInput_ID` int(11) DEFAULT NULL,
  `Estado` enum('none','ok','error') DEFAULT 'none',
  `Titulo` varchar(500) DEFAULT NULL COMMENT 'Nombre identificativo del tipo de info, si es de un input, un nodo o de central',
  `URL_WS_Status` varchar(1000) DEFAULT NULL COMMENT 'URL WS para consultar status',
  `URL_Config_Input` varchar(1000) DEFAULT NULL COMMENT 'URL del dispositivo para ver su configuración',
  `FechaOK_Input` datetime DEFAULT NULL COMMENT 'Actualizado por Scheduler(capturer) tras cada chunk OK insertado en SOLR',
  `FechaOK_Nodo` datetime DEFAULT NULL COMMENT 'Actualizado por tras cada ping con respuesta OK',
  `FechaOK_Scheduler` datetime DEFAULT NULL COMMENT 'Actualizado por Scheduler(capturer) tras cada chunk OK insertado en SOLR',
  `FechaOK_Analyser` datetime DEFAULT NULL COMMENT 'Actualizado por Analyser tras cada operacion OK',
  `FechaOK_SOLR` datetime DEFAULT NULL COMMENT 'Actualizado por tras cada ping con respuesta OK',
  `FechaOK_Web` datetime DEFAULT NULL COMMENT 'Actualizado por tras cada ping con respuesta OK',
  `Info_Disk` int(11) DEFAULT NULL COMMENT 'Porcentaje de ocupacion de disco',
  `Info_RAM` int(11) DEFAULT NULL COMMENT 'Porcentaje de ocupacion de memoria',
  `Info_CPU` int(11) DEFAULT NULL COMMENT 'Porcentaje de ocupacion de CPU',
  `Info_GPU` int(11) DEFAULT NULL COMMENT 'Porcentaje de ocupacion de GPU',
  `FechaOK_Info` datetime DEFAULT NULL,
  `bActivo` tinyint(4) unsigned NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`StatusInfo_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_tesauro_info`
--

DROP TABLE IF EXISTS `tb_tesauro_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_tesauro_info` (
  `TesauroInfo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Tags` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Codigo` int(11) unsigned NOT NULL,
  `OID` int(11) unsigned NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`TesauroInfo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=29893 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_catalog_task`
--

DROP TABLE IF EXISTS `tb_video_catalog_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_catalog_task` (
  `VideoCatalogTask_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID` int(11) DEFAULT NULL,
  `Hidden` int(1) DEFAULT 0,
  `UserID` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `Priority` int(11) DEFAULT NULL,
  `ExecuteOrder` int(11) DEFAULT NULL,
  `VideoName` varchar(500) COLLATE utf8_spanish_ci DEFAULT '',
  `FullVideoPath` varchar(2000) COLLATE utf8_spanish_ci DEFAULT '',
  `ExternalStreamingUrl` varchar(2000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CatalogProfileID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `broadcastingDate` datetime DEFAULT NULL,
  `TaskState` int(11) DEFAULT NULL,
  `ErrorMessage` varchar(4000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ChannelID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CurrentProcessingFrame` int(11) DEFAULT NULL,
  `FinalFrame` int(11) DEFAULT NULL,
  `StartTime` datetime DEFAULT NULL,
  `LastUpdateTime` datetime DEFAULT NULL,
  `FinishTime` double DEFAULT NULL,
  `TempDirectory` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `InternalProcessPID` int(11) DEFAULT NULL,
  `InternalServerClientID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `InternalAudioProcessPID` int(11) DEFAULT NULL,
  `InternalASRImageID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `VideoTitle` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Locale` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `NumberOfRetries` int(11) DEFAULT NULL,
  `Duration` int(11) DEFAULT -1,
  PRIMARY KEY (`VideoCatalogTask_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_codec`
--

DROP TABLE IF EXISTS `tb_video_codec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_codec` (
  `VideoCodec_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `VideoCodecID` varchar(255) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'Campo Name del XML',
  `Titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'MPEG4',
  `LibreriaCodec` varchar(255) COLLATE utf8_spanish_ci DEFAULT 'libx264',
  `IsGPU` tinyint(4) NOT NULL DEFAULT 0,
  `Orden` int(11) unsigned NOT NULL DEFAULT 9999,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`VideoCodec_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=4096;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_editor_task`
--

DROP TABLE IF EXISTS `tb_video_editor_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_editor_task` (
  `VideoEditorTask_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Server_ID` int(11) DEFAULT NULL,
  `ID` int(11) DEFAULT NULL,
  `Hidden` int(1) DEFAULT 0,
  `UserID` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `Priority` int(11) DEFAULT NULL,
  `ExecuteOrder` int(11) DEFAULT NULL,
  `VideoName` varchar(500) COLLATE utf8_spanish_ci DEFAULT '',
  `FullVideoPath` varchar(2000) COLLATE utf8_spanish_ci DEFAULT '',
  `VideoFragments` varchar(5000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TranscodingProfileID` varchar(2000) COLLATE utf8_spanish_ci DEFAULT '',
  `outputFolderPath` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `TaskState` int(11) DEFAULT NULL,
  `ErrorMessage` varchar(4000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `AccumulatedFramesUntilLastVideo` int(11) DEFAULT NULL,
  `CurrentProcessingFrame` int(11) DEFAULT NULL,
  `FinalFrame` int(11) DEFAULT NULL,
  `VideoTime` int(11) DEFAULT NULL,
  `StartTime` datetime DEFAULT NULL,
  `LastUpdateTime` datetime DEFAULT NULL,
  `ElapsedTime` double DEFAULT NULL,
  `FinishTime` double DEFAULT NULL,
  `InternalProcessPID` int(11) DEFAULT NULL,
  `NumberOfRetries` int(11) DEFAULT NULL,
  `ClipInfo_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`VideoEditorTask_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_transcoding_task`
--

DROP TABLE IF EXISTS `tb_video_transcoding_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_transcoding_task` (
  `VideoTranscodingTask_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Server_ID` int(11) DEFAULT NULL,
  `GPU_ID` int(11) DEFAULT NULL,
  `ID` int(11) DEFAULT NULL,
  `Hidden` int(1) DEFAULT 0,
  `UserID` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `Priority` int(11) DEFAULT NULL,
  `ExecuteOrder` int(11) DEFAULT NULL,
  `VideoName` varchar(500) COLLATE utf8_spanish_ci DEFAULT '',
  `FullVideoPath` varchar(2000) COLLATE utf8_spanish_ci DEFAULT '',
  `TranscodingProfileID` varchar(2000) COLLATE utf8_spanish_ci DEFAULT '',
  `outputFileName` varchar(2000) COLLATE utf8_spanish_ci DEFAULT '',
  `outputFolderPath` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `TaskState` int(11) DEFAULT NULL,
  `ErrorMessage` varchar(1000) COLLATE utf8_spanish_ci DEFAULT '',
  `ChannelID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CurrentProcessingFrame` int(11) DEFAULT NULL,
  `FinalFrame` int(11) DEFAULT NULL,
  `VideoTime` int(11) DEFAULT NULL,
  `StartTime` datetime DEFAULT NULL,
  `LastUpdateTime` datetime DEFAULT NULL,
  `FinishTime` double DEFAULT NULL,
  `InternalProcessPID` int(11) DEFAULT NULL,
  `InternalTranscodServerID` int(11) DEFAULT NULL,
  `IniTime` int(64) DEFAULT NULL,
  `EndTime` int(64) DEFAULT NULL,
  `AudioTracksFullPath` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `IsSenado` int(1) DEFAULT NULL,
  `FTP_Id` int(11) DEFAULT NULL,
  `UploadToFtp` int(1) DEFAULT NULL,
  `FtpRemotePath` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FtpPasteMode` int(1) DEFAULT NULL,
  `NumberOfRetries` int(11) DEFAULT NULL,
  `FTPIdsList` varchar(2000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `YoutubeChannelIdsList` varchar(2000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `MergingRefVideoIndex` int(11) NOT NULL DEFAULT 0,
  `NeedFrames` int(1) DEFAULT 0,
  `UseWatermark` varchar(2000) COLLATE utf8_spanish_ci DEFAULT '',
  `WatermarkFile` varchar(2000) COLLATE utf8_spanish_ci DEFAULT '',
  `WatermarkPosition` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `HlsEncapsulated` varchar(2000) COLLATE utf8_spanish_ci DEFAULT '',
  `EncapsulationTime` int(11) DEFAULT 0,
  `AudioMappings` varchar(1000) COLLATE utf8_spanish_ci DEFAULT '',
  PRIMARY KEY (`VideoTranscodingTask_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1731 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_content_type_info`
--

DROP TABLE IF EXISTS `tm_content_type_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_content_type_info` (
  `ContentTypeInfo_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TipoContentType_ID` enum('Video','Foto','Sonido') DEFAULT NULL,
  `CodecVideo_ID` int(11) DEFAULT 0,
  `CodecAudio_ID` int(11) DEFAULT 0,
  `CodecPhoto_ID` int(11) DEFAULT 0,
  PRIMARY KEY (`ContentTypeInfo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 AVG_ROW_LENGTH=8192;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_event`
--

DROP TABLE IF EXISTS `tm_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_event` (
  `Event_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Sender_ID` int(11) NOT NULL,
  `SenderType` enum('supertask','task','subtask') NOT NULL DEFAULT 'task',
  `Date` datetime NOT NULL DEFAULT current_timestamp(),
  `Title` varchar(255) NOT NULL DEFAULT '',
  `Value` text NOT NULL,
  PRIMARY KEY (`Event_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_license_info`
--

DROP TABLE IF EXISTS `tm_license_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_license_info` (
  `License_ID` int(11) NOT NULL,
  `Titulo` varchar(500) DEFAULT NULL,
  `FechaIni` datetime DEFAULT NULL,
  `FechaFin` datetime DEFAULT NULL,
  `MinutosTotal` int(11) NOT NULL DEFAULT 0,
  `MinutosConsumidos` int(11) NOT NULL DEFAULT 0,
  `bPeriodica` tinyint(4) NOT NULL DEFAULT 1,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`License_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AVG_ROW_LENGTH=16384;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_manager`
--

DROP TABLE IF EXISTS `tm_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_manager` (
  `Manager_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TipoEngine_ID` enum('live','indexing','transcoding','analysis','capture','streaming','download','upload','notification') DEFAULT NULL,
  `Title` varchar(255) DEFAULT '',
  `MaxConcurrentTasks` int(11) DEFAULT 1,
  `MaxRetries` int(11) DEFAULT 1,
  `bActive` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`Manager_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 AVG_ROW_LENGTH=1365;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_server_app`
--

DROP TABLE IF EXISTS `tm_server_app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_server_app` (
  `ServerApp_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Server_ID` int(11) unsigned NOT NULL,
  `Title` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaActualizacion` datetime DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`ServerApp_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_server_info`
--

DROP TABLE IF EXISTS `tm_server_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_server_info` (
  `Server_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(500) CHARACTER SET utf8 DEFAULT '',
  `IP_Address` varchar(50) CHARACTER SET utf8 DEFAULT '',
  `FechaActualizacion` datetime DEFAULT NULL COMMENT 'Ãšltima comprobacion de estado del servidor',
  `Status_SecsToGetFreeSlot` int(11) unsigned NOT NULL DEFAULT 0 COMMENT 'Tiempo hasta tener un slot mas de licencia libre',
  `Status_NumFreeSlots` int(11) unsigned NOT NULL DEFAULT 0,
  `TotalSlots` int(11) DEFAULT 0,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Server_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_subtask`
--

DROP TABLE IF EXISTS `tm_subtask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_subtask` (
  `SubTask_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) DEFAULT '',
  `FechaAlta` datetime DEFAULT current_timestamp(),
  `FechaIni` datetime DEFAULT NULL,
  `FechaFin` datetime DEFAULT NULL,
  `Manager_ID` int(11) DEFAULT 0,
  `GenerateErrorEvent` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'En caso de ser true, la SubTarea generará una o varias SubTareas en caso de acabar la SuperTarea como erronea',
  `DataJSON` text DEFAULT NULL COMMENT 'JSON data',
  PRIMARY KEY (`SubTask_ID`),
  KEY `tm_subtask_ibfk_1` (`Manager_ID`),
  CONSTRAINT `tm_subtask_ibfk_1` FOREIGN KEY (`Manager_ID`) REFERENCES `tm_manager` (`Manager_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11806 DEFAULT CHARSET=utf8mb4 AVG_ROW_LENGTH=603;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_subtask_intent`
--

DROP TABLE IF EXISTS `tm_subtask_intent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_subtask_intent` (
  `SubTask_Intent_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SuperTask_Task_SubTask_ID` int(11) DEFAULT 0,
  `SubTask_ID` int(11) DEFAULT 0,
  `FechaIni` datetime DEFAULT NULL,
  `FechaFin` datetime DEFAULT NULL,
  `Error` text DEFAULT NULL,
  `TipoState_ID` enum('waiting','running','completed','error','superError','aborted','managerClosed','unknown') DEFAULT 'waiting',
  `Progress` double DEFAULT 0,
  PRIMARY KEY (`SubTask_Intent_ID`),
  KEY `tm_subtask_intent_ibfk_1` (`SuperTask_Task_SubTask_ID`),
  KEY `tm_subtask_intent_ibfk_2` (`SubTask_ID`),
  CONSTRAINT `tm_subtask_intent_ibfk_1` FOREIGN KEY (`SuperTask_Task_SubTask_ID`) REFERENCES `tm_supertask_task_subtask` (`SuperTask_Task_SubTask_ID`),
  CONSTRAINT `tm_subtask_intent_ibfk_2` FOREIGN KEY (`SubTask_ID`) REFERENCES `tm_subtask` (`SubTask_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12696 DEFAULT CHARSET=utf8mb4 AVG_ROW_LENGTH=188;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_supertask`
--

DROP TABLE IF EXISTS `tm_supertask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_supertask` (
  `SuperTask_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Server_ID` int(11) DEFAULT NULL,
  `Title` varchar(255) DEFAULT '',
  `Tipologia_ID` enum('ninguno','capture','live-analysis','indexing','transcoding','streaming','postprocess') NOT NULL DEFAULT 'ninguno',
  `FechaAlta` datetime DEFAULT current_timestamp(),
  `FechaIni` datetime DEFAULT NULL,
  `FechaFin` datetime DEFAULT NULL,
  `WorkingDir` varchar(255) DEFAULT '',
  `Priority` int(11) DEFAULT 1,
  `Orden` int(11) DEFAULT 99999999,
  `CurrentOrderTask` int(11) DEFAULT 0,
  `ContentID` varchar(255) DEFAULT '',
  `ContentType_ID` enum('Video','Foto','Sonido') DEFAULT NULL,
  `ContentTypeInfo_ID` int(11) DEFAULT 0,
  `ContentFile` varchar(255) DEFAULT '',
  `ContentPath` varchar(255) DEFAULT '',
  `ContentURL` varchar(255) DEFAULT '',
  `ContentThumbnailURL` varchar(255) DEFAULT '',
  `PercentDone` int(11) DEFAULT 0,
  `RemainingTime` int(11) DEFAULT 0 COMMENT 'Se devuelven segundos',
  `EstimatedTime` int(11) DEFAULT 0 COMMENT 'Se devuelven segundos',
  `WorkFlow_ID` int(11) DEFAULT 0,
  `WorkFlow_Modo` enum('captura-video','captura-audio','fichero-video','fichero-audio','fichero-foto') NOT NULL DEFAULT 'captura-video',
  `WorkFlow_Titulo` varchar(500) DEFAULT NULL COMMENT 'Titulo del workflow para mostrar en tarea',
  `WorkFlow_Info` varchar(3000) DEFAULT '' COMMENT 'Informacion de Fuentes o Destinos a mostrar en listado',
  `TipoState_ID` enum('waiting','running','completed','error','abortWaiting','aborted','managerClosed','creating','unknown','deleteWaiting','deleted','relaunchWaiting','relaunching','resetWaiting','reseted') DEFAULT 'creating',
  `User_ID` int(11) DEFAULT 0,
  `ListOfTaskProgress` text DEFAULT NULL,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `InfoProgram_PID` varchar(50) DEFAULT NULL,
  `InfoProgram_Name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `InfoProgram_Code` varchar(50) DEFAULT NULL COMMENT 'Hace referencia a tb_info_workflow_channel',
  `FeedInfo_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'Si tm_supertask.FeedInfo_ID > 0 se muestran tiempos y thumb de feed_info, sino los de tm_supertask en Listados de recordings.',
  PRIMARY KEY (`SuperTask_ID`),
  KEY `WorkFlow_ID` (`WorkFlow_ID`),
  KEY `tm_supertask_ibfk_1` (`ContentTypeInfo_ID`),
  CONSTRAINT `tm_supertask_ibfk_1` FOREIGN KEY (`ContentTypeInfo_ID`) REFERENCES `tm_content_type_info` (`ContentTypeInfo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4536 DEFAULT CHARSET=utf8mb4 AVG_ROW_LENGTH=1337;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_supertask_blackboard`
--

DROP TABLE IF EXISTS `tm_supertask_blackboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_supertask_blackboard` (
  `SuperTask_BlackBoard_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SuperTask_ID` int(11) DEFAULT 0,
  `SubTask_Intent_ID` int(11) DEFAULT 0,
  `FechaAlta` datetime DEFAULT current_timestamp(),
  `Title` varchar(255) DEFAULT '',
  `Value` text DEFAULT NULL,
  PRIMARY KEY (`SuperTask_BlackBoard_ID`),
  KEY `tm_supertask_blackboard_ibfk_1` (`SuperTask_ID`),
  KEY `tm_supertask_blackboard_ibfk_2` (`SubTask_Intent_ID`),
  CONSTRAINT `tm_supertask_blackboard_ibfk_1` FOREIGN KEY (`SuperTask_ID`) REFERENCES `tm_supertask` (`SuperTask_ID`),
  CONSTRAINT `tm_supertask_blackboard_ibfk_2` FOREIGN KEY (`SubTask_Intent_ID`) REFERENCES `tm_subtask_intent` (`SubTask_Intent_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4589 DEFAULT CHARSET=utf8mb4 AVG_ROW_LENGTH=682;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_supertask_step`
--

DROP TABLE IF EXISTS `tm_supertask_step`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_supertask_step` (
  `SuperTask_Step_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SuperTask_ID` int(11) DEFAULT 0,
  `TaskStep` int(11) DEFAULT 0,
  `Title` varchar(255) DEFAULT '',
  `FechaAlta` datetime DEFAULT current_timestamp(),
  `bCurrent` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`SuperTask_Step_ID`),
  KEY `tm_supertask_step_ibfk_1` (`SuperTask_ID`),
  CONSTRAINT `tm_supertask_step_ibfk_1` FOREIGN KEY (`SuperTask_ID`) REFERENCES `tm_supertask` (`SuperTask_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8936 DEFAULT CHARSET=utf8mb4 AVG_ROW_LENGTH=420;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_supertask_task_step`
--

DROP TABLE IF EXISTS `tm_supertask_task_step`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_supertask_task_step` (
  `SuperTask_Task_Step_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SuperTask_Step_ID` int(11) DEFAULT 0,
  `SuperTask_ID` int(11) DEFAULT 0,
  `Task_ID` int(11) DEFAULT 0,
  PRIMARY KEY (`SuperTask_Task_Step_ID`),
  KEY `tm_supertask_task_step_ibfk_1` (`SuperTask_ID`),
  KEY `tm_supertask_task_step_ibfk_2` (`Task_ID`),
  KEY `tm_supertask_task_step_ibfk_3` (`SuperTask_Step_ID`),
  CONSTRAINT `tm_supertask_task_step_ibfk_1` FOREIGN KEY (`SuperTask_ID`) REFERENCES `tm_supertask` (`SuperTask_ID`),
  CONSTRAINT `tm_supertask_task_step_ibfk_2` FOREIGN KEY (`Task_ID`) REFERENCES `tm_task` (`Task_ID`),
  CONSTRAINT `tm_supertask_task_step_ibfk_3` FOREIGN KEY (`SuperTask_Step_ID`) REFERENCES `tm_supertask_step` (`SuperTask_Step_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9142 DEFAULT CHARSET=utf8mb4 AVG_ROW_LENGTH=143;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_supertask_task_subtask`
--

DROP TABLE IF EXISTS `tm_supertask_task_subtask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_supertask_task_subtask` (
  `SuperTask_Task_SubTask_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SuperTask_ID` int(11) DEFAULT 0,
  `Task_ID` int(11) DEFAULT 0,
  `SubTask_ID` int(11) DEFAULT 0,
  `Title` varchar(255) DEFAULT '',
  `FechaAlta` datetime DEFAULT current_timestamp(),
  `Desc` text DEFAULT NULL,
  `TipoState_ID` enum('waiting','running','completed','error','superError','aborted','managerClosed','unknown') DEFAULT 'waiting',
  `Progress` double DEFAULT 0,
  PRIMARY KEY (`SuperTask_Task_SubTask_ID`),
  KEY `tm_supertask_task_subtask_ibfk_1` (`SuperTask_ID`),
  KEY `tm_supertask_task_subtask_ibfk_2` (`Task_ID`),
  KEY `tm_supertask_task_subtask_ibfk_3` (`SubTask_ID`),
  CONSTRAINT `tm_supertask_task_subtask_ibfk_1` FOREIGN KEY (`SuperTask_ID`) REFERENCES `tm_supertask` (`SuperTask_ID`),
  CONSTRAINT `tm_supertask_task_subtask_ibfk_2` FOREIGN KEY (`Task_ID`) REFERENCES `tm_task` (`Task_ID`),
  CONSTRAINT `tm_supertask_task_subtask_ibfk_3` FOREIGN KEY (`SubTask_ID`) REFERENCES `tm_subtask` (`SubTask_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11803 DEFAULT CHARSET=utf8mb4 AVG_ROW_LENGTH=431;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_task`
--

DROP TABLE IF EXISTS `tm_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_task` (
  `Task_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SuperTask_ID` int(11) DEFAULT 0,
  `DispositivoInput_ID` int(11) DEFAULT NULL,
  `Title` varchar(255) DEFAULT '',
  `FechaAlta` datetime DEFAULT current_timestamp(),
  `FechaIni` datetime DEFAULT NULL,
  `FechaFin` datetime DEFAULT NULL,
  `TipoEngine_ID` enum('live','indexing','transcoding','analysis','capture','streaming','download','upload','notification') DEFAULT 'live',
  `TipoState_ID` enum('waiting','running','completed','error','abortWaiting','aborted','managerClosed','creating','unknown','deleteWaiting','deleted','relaunchWaiting','relaunching','resetWaiting','reseted') DEFAULT 'waiting',
  `PercentDone` int(11) DEFAULT 0,
  `RemainingTime` int(11) DEFAULT 0 COMMENT 'Se devuelven segundos',
  `ListOfSubTaskProgress` text DEFAULT NULL,
  `ContentID` varchar(255) DEFAULT '',
  `ContentType_ID` enum('Video','Foto','Sonido') DEFAULT 'Video',
  `ContentTypeInfo_ID` int(11) DEFAULT 0,
  `ContentFile` varchar(255) DEFAULT '',
  `ContentPath` varchar(255) DEFAULT '',
  `ContentURL` varchar(255) DEFAULT '',
  `ContentThumbnailURL` varchar(255) DEFAULT '',
  PRIMARY KEY (`Task_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9142 DEFAULT CHARSET=utf8mb4 AVG_ROW_LENGTH=143;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_task_step`
--

DROP TABLE IF EXISTS `tm_task_step`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_task_step` (
  `Task_Step_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Task_ID` int(11) DEFAULT 0,
  `SubTaskStep` int(11) DEFAULT 0,
  `Title` varchar(255) DEFAULT '',
  `FechaAlta` datetime DEFAULT current_timestamp(),
  `bCurrent` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`Task_Step_ID`),
  KEY `tm_task_step_ibfk_1` (`Task_ID`),
  CONSTRAINT `tm_task_step_ibfk_1` FOREIGN KEY (`Task_ID`) REFERENCES `tm_task` (`Task_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10934 DEFAULT CHARSET=utf8mb4 AVG_ROW_LENGTH=86;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_task_subtask_step`
--

DROP TABLE IF EXISTS `tm_task_subtask_step`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_task_subtask_step` (
  `Task_SubTask_Step_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Task_Step_ID` int(11) DEFAULT 0,
  `Task_ID` int(11) DEFAULT 0,
  `SubTask_ID` int(11) DEFAULT 0,
  PRIMARY KEY (`Task_SubTask_Step_ID`),
  KEY `tm_task_subtask_step_ibfk_1` (`Task_ID`),
  KEY `tm_task_subtask_step_ibfk_2` (`SubTask_ID`),
  KEY `tm_task_subtask_step_ibfk_3` (`Task_Step_ID`),
  CONSTRAINT `tm_task_subtask_step_ibfk_1` FOREIGN KEY (`Task_ID`) REFERENCES `tm_task` (`Task_ID`),
  CONSTRAINT `tm_task_subtask_step_ibfk_2` FOREIGN KEY (`SubTask_ID`) REFERENCES `tm_subtask` (`SubTask_ID`),
  CONSTRAINT `tm_task_subtask_step_ibfk_3` FOREIGN KEY (`Task_Step_ID`) REFERENCES `tm_task_step` (`Task_Step_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11804 DEFAULT CHARSET=utf8mb4 AVG_ROW_LENGTH=86;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tm_workflow_blackboard`
--

DROP TABLE IF EXISTS `tm_workflow_blackboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_workflow_blackboard` (
  `WorkFlow_BlackBoard_ID` int(11) NOT NULL AUTO_INCREMENT,
  `WorkFlow_ID` int(11) DEFAULT 0,
  `SubTask_Intent_ID` int(11) DEFAULT 0,
  `FechaAlta` datetime DEFAULT current_timestamp(),
  `Title` varchar(255) DEFAULT '',
  `Value` text DEFAULT NULL,
  PRIMARY KEY (`WorkFlow_BlackBoard_ID`),
  KEY `SuperTask_ID` (`WorkFlow_ID`),
  KEY `tm_workflow_blackboard_ibfk_2` (`SubTask_Intent_ID`),
  CONSTRAINT `tm_workflow_blackboard_ibfk_1` FOREIGN KEY (`WorkFlow_ID`) REFERENCES `tm_supertask` (`WorkFlow_ID`),
  CONSTRAINT `tm_workflow_blackboard_ibfk_2` FOREIGN KEY (`SubTask_Intent_ID`) REFERENCES `tm_subtask_intent` (`SubTask_Intent_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-08 11:49:17
