-- MySQL dump 10.16  Distrib 10.1.21-MariaDB, for Win32 (AMD64)
--
-- Host: 10.5.13.24    Database: 10.5.13.24
-- ------------------------------------------------------
-- Server version	10.4.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `base_categoria`
--

DROP TABLE IF EXISTS `base_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_categoria` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Categoria_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CodigoAPP` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `CategoriaPadre_ID` int(11) NOT NULL DEFAULT 0,
  `Foto1_ID` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `ERP_ID` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Categoria_ID`),
  UNIQUE KEY `Categoria_ID_IND_U` (`Categoria_ID`),
  KEY `Foto1_ID_ind` (`Foto1_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=224 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_ccaa_es`
--

DROP TABLE IF EXISTS `base_ccaa_es`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_ccaa_es` (
  `CCAA_ID` tinyint(4) NOT NULL,
  `Titulo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `COD` char(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`CCAA_ID`),
  UNIQUE KEY `CCAA_ID_IND_U` (`CCAA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=862 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_contenido`
--

DROP TABLE IF EXISTS `base_contenido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_contenido` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Contenido_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `Titulo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Subtitulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Entradilla` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Descripcion` text CHARACTER SET utf8 DEFAULT NULL,
  `Texto` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Area_ID` int(11) NOT NULL DEFAULT 0,
  `Tipo_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'Tipo de contenido',
  `URL` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `Foto1_ID` int(11) DEFAULT NULL,
  `Foto2_ID` int(11) DEFAULT NULL,
  `Galeria1_ID` int(11) DEFAULT NULL,
  `Linea1` varchar(250) COLLATE utf8_spanish_ci DEFAULT '0',
  `Linea2` varchar(250) COLLATE utf8_spanish_ci DEFAULT '0',
  `Linea3` varchar(250) COLLATE utf8_spanish_ci DEFAULT '0',
  `FechaAlta` datetime NOT NULL,
  `FechaBaja` datetime DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  `Tags` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TipoAux_ID` int(11) DEFAULT NULL COMMENT 'Subtipo para categorizar',
  `bActivo` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Contenido_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=4311;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_contenido_fichero`
--

DROP TABLE IF EXISTS `base_contenido_fichero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_contenido_fichero` (
  `Contenido_ID` int(11) NOT NULL,
  `Fichero_ID` int(11) NOT NULL,
  PRIMARY KEY (`Contenido_ID`,`Fichero_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_entidad_grupo_contenido`
--

DROP TABLE IF EXISTS `base_entidad_grupo_contenido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_entidad_grupo_contenido` (
  `Entidad_ID` int(11) unsigned NOT NULL,
  `GrupoContenido_ID` int(11) unsigned NOT NULL,
  `RolAcceso_ID` int(11) DEFAULT 9 COMMENT 'Indica el Tipo de Entidad (base_usuario, tb_cliente, etc.) al que se asocia el Grupo',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Entidad_ID`,`GrupoContenido_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=2048 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_entidad_idioma`
--

DROP TABLE IF EXISTS `base_entidad_idioma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_entidad_idioma` (
  `EntidadIdioma_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Entidad_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `Idioma_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `Tabla` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `Campo` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Campo a reemplazar de tbReceta con el texto aqui definido. ej. "Titulo"',
  `Valor` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `bActual` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`EntidadIdioma_ID`),
  UNIQUE KEY `EntidadIdioma_ID_IND_U` (`EntidadIdioma_ID`),
  KEY `Entidad_ID_IND` (`Entidad_ID`),
  KEY `Idioma_ID_IND` (`Idioma_ID`),
  CONSTRAINT `base_entidad_idioma_ibfk_1` FOREIGN KEY (`Idioma_ID`) REFERENCES `base_idioma` (`Idioma_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_fichero`
--

DROP TABLE IF EXISTS `base_fichero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_fichero` (
  `Fichero_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Usuario_ID` int(11) DEFAULT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `RutaWeb` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `RutaSystem` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Tamano` int(11) DEFAULT NULL,
  `Formato` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FicheroDatos` longblob DEFAULT NULL,
  `FechaAlta` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `TipoEntidad_ID` int(11) NOT NULL DEFAULT 0,
  `Entidad_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Fichero_ID`),
  UNIQUE KEY `Fichero_ID_IND_U` (`Fichero_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3017 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=297 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_galeria`
--

DROP TABLE IF EXISTS `base_galeria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_galeria` (
  `Galeria_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `Descripcion` text CHARACTER SET utf8 DEFAULT NULL,
  `Tipo_ID` int(11) DEFAULT NULL,
  `URL` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `Foto1_ID` int(11) DEFAULT NULL,
  `Foto2_ID` int(11) DEFAULT NULL,
  `Linea1` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Linea2` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Linea3` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaAlta` datetime NOT NULL,
  `FechaBaja` datetime DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Galeria_ID`),
  UNIQUE KEY `Galeria_ID_IND_U` (`Galeria_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18464 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=78 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_galeria_fichero`
--

DROP TABLE IF EXISTS `base_galeria_fichero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_galeria_fichero` (
  `Galeria_ID` int(11) NOT NULL,
  `Fichero_ID` int(11) NOT NULL,
  PRIMARY KEY (`Galeria_ID`,`Fichero_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1024 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_idioma`
--

DROP TABLE IF EXISTS `base_idioma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_idioma` (
  `Idioma_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Codigo` char(2) COLLATE utf8_spanish_ci NOT NULL,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Idioma_ID`),
  UNIQUE KEY `Idioma_ID_IND_U` (`Idioma_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=8192 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_language_code_i18n`
--

DROP TABLE IF EXISTS `base_language_code_i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_language_code_i18n` (
  `3Letras_COD` char(3) CHARACTER SET utf8 NOT NULL COMMENT 'ISO 639-2 Code',
  `2Letras_COD` varchar(2) CHARACTER SET utf8 DEFAULT NULL COMMENT 'ISO 639-1 Code',
  `Titulo_ES` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Titulo_EN` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Titulo_FR` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`3Letras_COD`),
  KEY `2Letras_COD` (`2Letras_COD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_log_acceso_cms`
--

DROP TABLE IF EXISTS `base_log_acceso_cms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_log_acceso_cms` (
  `LogAccesoPanel_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Usuario_ID` int(11) NOT NULL DEFAULT 0,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `IP` varchar(15) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Fecha` datetime DEFAULT NULL,
  `Login` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Password` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Acceso` varchar(32) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`LogAccesoPanel_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3972 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=134 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_log_acceso_front`
--

DROP TABLE IF EXISTS `base_log_acceso_front`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_log_acceso_front` (
  `LogAccesoPanel_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Usuario_ID` int(11) NOT NULL DEFAULT 0,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `IP` varchar(15) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Fecha` datetime DEFAULT NULL,
  `Login` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Password` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Acceso` varchar(32) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `UserAgent` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SO` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Navegador` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bError` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`LogAccesoPanel_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4106 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=392 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_municipio_es`
--

DROP TABLE IF EXISTS `base_municipio_es`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_municipio_es` (
  `Municipio_ID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `Provincia_COD` char(2) COLLATE utf8_spanish_ci NOT NULL,
  `Municipio_COD` char(3) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Código de muncipio DENTRO de la provincia, campo no único',
  `DC` int(11) NOT NULL COMMENT 'Digito Control. El INE no revela cómo se calcula, secreto nuclear.',
  `Titulo` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`Municipio_ID`),
  UNIQUE KEY `Municipio_ID_IND_U` (`Municipio_ID`),
  KEY `Provincia_COD_IND` (`Provincia_COD`)
) ENGINE=InnoDB AUTO_INCREMENT=8117 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=52 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_pais`
--

DROP TABLE IF EXISTS `base_pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_pais` (
  `Pais_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(64) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Alpha_2` varchar(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Alpha_3` varchar(3) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 0,
  `Orden` int(11) NOT NULL DEFAULT 0,
  `Continente_ID` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Pais_ID`),
  KEY `countries_id` (`Pais_ID`),
  KEY `IDX_COUNTRIES_NAME` (`Nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=72 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_pais_es`
--

DROP TABLE IF EXISTS `base_pais_es`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_pais_es` (
  `Pais_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Alpha_2` varchar(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Alpha_3` varchar(3) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bActivo` tinyint(4) DEFAULT 0,
  `Orden` int(11) NOT NULL DEFAULT 0,
  `Continente_ID` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Pais_ID`),
  KEY `countries_id` (`Pais_ID`),
  KEY `IDX_COUNTRIES_NAME` (`Titulo`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=72 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_pais_iso_3166_1_alpha_2`
--

DROP TABLE IF EXISTS `base_pais_iso_3166_1_alpha_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_pais_iso_3166_1_alpha_2` (
  `Pais_COD` char(2) COLLATE utf8_spanish_ci NOT NULL COMMENT 'ISO 3166-1 alpha-2 code',
  `Titulo` varchar(70) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'English name of the country',
  `TLD` varchar(7) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Top Level Domain(s). UK has two TLDs!',
  PRIMARY KEY (`Pais_COD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=65 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_provincia`
--

DROP TABLE IF EXISTS `base_provincia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_provincia` (
  `Provincia_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Pais_ID` int(11) DEFAULT 0,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Provincia_ID`),
  UNIQUE KEY `Provincia_ID_IND_U` (`Provincia_ID`),
  KEY `Pais_ID` (`Pais_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3390 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=53 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_provincia_es`
--

DROP TABLE IF EXISTS `base_provincia_es`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_provincia_es` (
  `Provincia_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Provincia_COD` char(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CCAA_ID` tinyint(4) DEFAULT NULL,
  `World_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Provincia_ID`),
  UNIQUE KEY `Provincia_ID_IND_U` (`Provincia_ID`),
  KEY `CCAA_ID_IND` (`CCAA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=315 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_rol_acceso`
--

DROP TABLE IF EXISTS `base_rol_acceso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_rol_acceso` (
  `RolAcceso_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`RolAcceso_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`RolAcceso_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=2340 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_rol_acceso_area`
--

DROP TABLE IF EXISTS `base_rol_acceso_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_rol_acceso_area` (
  `RolAcceso_ID` int(11) NOT NULL,
  `Area_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`RolAcceso_ID`,`Area_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=682 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_rol_acceso_cms`
--

DROP TABLE IF EXISTS `base_rol_acceso_cms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_rol_acceso_cms` (
  `RolAcceso_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`RolAcceso_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`RolAcceso_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1092 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_rol_acceso_front`
--

DROP TABLE IF EXISTS `base_rol_acceso_front`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_rol_acceso_front` (
  `RolAcceso_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`RolAcceso_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`RolAcceso_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=4096 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_system`
--

DROP TABLE IF EXISTS `base_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_system` (
  `Accion_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `FechaAlta` datetime DEFAULT NULL,
  `FechaUltimoAcceso` datetime DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Accion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=8192 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_tipo`
--

DROP TABLE IF EXISTS `base_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_tipo` (
  `Tipo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CategoriaPadre_ID` int(11) NOT NULL DEFAULT 0,
  `CodigoAPP` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Codigo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) DEFAULT 99999,
  `Foto1_ID` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Tipo_ID`),
  UNIQUE KEY `Tipo_Ind_ID_U` (`Tipo_ID`),
  KEY `bActivo_IND` (`bActivo`),
  KEY `Categoria_padre_ID` (`CategoriaPadre_ID`),
  KEY `Orden` (`Orden`)
) ENGINE=InnoDB AUTO_INCREMENT=11201 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=192 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_usuario_cms`
--

DROP TABLE IF EXISTS `base_usuario_cms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_usuario_cms` (
  `Usuario_ID` int(11) NOT NULL AUTO_INCREMENT,
  `GrupoUsuario_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'Diputados, Prensa, Ciudadanos, etc...',
  `RolAcceso_ID` int(11) NOT NULL DEFAULT 6 COMMENT 'Supervisor, Gestor, etc...',
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Login` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Password` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `FechaAlta` datetime DEFAULT NULL,
  `FechaUltimoAcceso` datetime DEFAULT NULL,
  `Foto1_ID` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `Estado_ID` int(11) DEFAULT 0 COMMENT '0 - NUEVO, 1 - PENDIENTE, 2 - VALIDADO',
  `PasswordVer` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Usuario_ID`),
  KEY `Empresa_ID_IND` (`Portal_ID`),
  KEY `UsuarioRol_ID_IND` (`GrupoUsuario_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=256 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_usuario_front`
--

DROP TABLE IF EXISTS `base_usuario_front`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_usuario_front` (
  `Usuario_ID` int(11) NOT NULL AUTO_INCREMENT,
  `GrupoUsuario_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'Diputados, Prensa, Ciudadanos, etc...',
  `RolAcceso_ID` int(11) NOT NULL DEFAULT 6 COMMENT 'Supervisor, Gestor, etc...',
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Login` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Password` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `Nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `FechaAlta` datetime DEFAULT NULL,
  `FechaExpiracion` datetime DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `FechaUltimoAcceso` datetime DEFAULT NULL,
  `Foto1_ID` int(11) DEFAULT NULL,
  `Foto2_ID` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `Estado_ID` int(11) DEFAULT 0 COMMENT '0 - NUEVO, 1 - PENDIENTE, 2 - VALIDADO',
  `PasswordVer` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `Origen` varchar(255) COLLATE utf8_spanish_ci DEFAULT 'MANUAL',
  PRIMARY KEY (`Usuario_ID`),
  KEY `Empresa_ID_IND` (`Portal_ID`),
  KEY `UsuarioRol_ID_IND` (`GrupoUsuario_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=383 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=546 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_usuario_front_ldap`
--

DROP TABLE IF EXISTS `base_usuario_front_ldap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_usuario_front_ldap` (
  `Usuario_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `GrupoUsuario_ID` int(11) DEFAULT 0 COMMENT 'Diputados, Prensa, Ciudadanos, etc...',
  `RolAcceso_ID` int(11) DEFAULT 0 COMMENT 'Supervisor, Gestor, etc...',
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `username` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `userpassword` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `location` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Usuario_ID`),
  KEY `Empresa_ID_IND` (`Portal_ID`),
  KEY `UsuarioRol_ID_IND` (`GrupoUsuario_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=606 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_usuario_grupo`
--

DROP TABLE IF EXISTS `base_usuario_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_usuario_grupo` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Grupo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Tipo_ID` int(11) DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `FechaExpiracion` datetime DEFAULT NULL,
  PRIMARY KEY (`Grupo_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`Grupo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=280 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=153 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_usuario_permiso`
--

DROP TABLE IF EXISTS `base_usuario_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_usuario_permiso` (
  `Portal_ID` int(11) NOT NULL DEFAULT 17,
  `Permiso_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Tipo_ID` int(11) DEFAULT NULL COMMENT 'Accion, Visualización, etc...',
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Permiso_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`Permiso_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=5461 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_usuario_token`
--

DROP TABLE IF EXISTS `base_usuario_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_usuario_token` (
  `Portal_ID` int(11) NOT NULL DEFAULT 17,
  `Session` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `Token` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Login` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Expire` datetime DEFAULT NULL,
  PRIMARY KEY (`Session`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `portal_tipo`
--

DROP TABLE IF EXISTS `portal_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portal_tipo` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Tipo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CategoriaPadre_ID` int(11) NOT NULL DEFAULT 0,
  `CodigoAPP` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Codigo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) DEFAULT 99999,
  `Foto1_ID` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Tipo_ID`),
  UNIQUE KEY `Tipo_Ind_ID_U` (`Tipo_ID`),
  KEY `bActivo_IND` (`bActivo`),
  KEY `Categoria_padre_ID` (`CategoriaPadre_ID`),
  KEY `Orden` (`Orden`)
) ENGINE=InnoDB AUTO_INCREMENT=7056 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=94 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb__categoria`
--

DROP TABLE IF EXISTS `tb__categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb__categoria` (
  `Categoria_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) NOT NULL DEFAULT 0,
  `CategoriaPadre_ID` int(11) NOT NULL DEFAULT 0,
  `Foto1_ID` int(11) NOT NULL DEFAULT 0,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Categoria_ID`),
  UNIQUE KEY `Categoria_ID_IND_U` (`Categoria_ID`),
  KEY `Categoria_padre_ID` (`CategoriaPadre_ID`),
  KEY `Foto1_ID_ind` (`Foto1_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1820 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb__param_general`
--

DROP TABLE IF EXISTS `tb__param_general`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb__param_general` (
  `ParamGeneral_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ParamTipo_ID` int(10) unsigned DEFAULT NULL,
  `Entorno` int(11) NOT NULL DEFAULT 0 COMMENT '0: DEV - 1: PRE - 2: PRO',
  `Titulo` varchar(255) DEFAULT NULL,
  `Valor` varchar(500) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ParamGeneral_ID`),
  KEY `ParamTipo_ID` (`ParamTipo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1250 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=108 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb__tipo`
--

DROP TABLE IF EXISTS `tb__tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb__tipo` (
  `Tipo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SolrField` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TipoPadre_ID` int(11) NOT NULL DEFAULT 0,
  `CategoriaPadre_ID` int(11) NOT NULL DEFAULT 0,
  `Foto1_ID` int(11) DEFAULT 0,
  `Orden` int(11) NOT NULL DEFAULT 0,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Tipo_ID`),
  UNIQUE KEY `Tipo_Ind_ID_U` (`Tipo_ID`),
  KEY `Categoria_padre_ID` (`CategoriaPadre_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=132 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_grupo`
--

DROP TABLE IF EXISTS `tb_acceso_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_grupo` (
  `Portal_ID` int(11) NOT NULL DEFAULT 1,
  `AccesoGrupo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Tipo_ID` int(11) DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`AccesoGrupo_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`AccesoGrupo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=209 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=4096 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_grupo_permiso`
--

DROP TABLE IF EXISTS `tb_acceso_grupo_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_grupo_permiso` (
  `AccesoGrupo_ID` int(11) NOT NULL,
  `AccesoPortal_ID` int(11) NOT NULL DEFAULT 0,
  `AccesoPermiso_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`AccesoGrupo_ID`,`AccesoPortal_ID`,`AccesoPermiso_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=2048 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_grupo_video`
--

DROP TABLE IF EXISTS `tb_acceso_grupo_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_grupo_video` (
  `Portal_ID` int(11) NOT NULL DEFAULT 1001,
  `Video_ID` int(11) NOT NULL,
  `Grupo_ID` int(11) NOT NULL,
  PRIMARY KEY (`Video_ID`,`Grupo_ID`,`Portal_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_log`
--

DROP TABLE IF EXISTS `tb_acceso_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_log` (
  `AccesoLog_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Usuario_ID` int(11) NOT NULL DEFAULT 0,
  `Portal_ID` int(11) NOT NULL DEFAULT 1,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `IP` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `Fecha` datetime DEFAULT NULL,
  `Login` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Password` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Acceso` varchar(32) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `UserAgent` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SO` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Navegador` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bError` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`AccesoLog_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=933 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=424 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_permiso`
--

DROP TABLE IF EXISTS `tb_acceso_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_permiso` (
  `Portal_ID` int(11) NOT NULL DEFAULT 1,
  `AccesoPermiso_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Tipo_ID` int(11) DEFAULT NULL COMMENT 'Descarga, Compartir, etc...',
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`AccesoPermiso_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`AccesoPermiso_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=509 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=3276 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_rol_acceso_cms`
--

DROP TABLE IF EXISTS `tb_acceso_rol_acceso_cms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_rol_acceso_cms` (
  `RolAcceso_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`RolAcceso_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`RolAcceso_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=3276;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_rol_acceso_front`
--

DROP TABLE IF EXISTS `tb_acceso_rol_acceso_front`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_rol_acceso_front` (
  `RolAcceso_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`RolAcceso_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`RolAcceso_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=4096;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_usuario`
--

DROP TABLE IF EXISTS `tb_acceso_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_usuario` (
  `Portal_ID` int(11) NOT NULL DEFAULT 100 COMMENT 'Portal por defecto para redireccion tras login de usuario',
  `Usuario_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Login` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Password` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Email` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `RolAcceso_ID` enum('2','3','4','5','6','7') COLLATE utf8_spanish_ci NOT NULL DEFAULT '7' COMMENT 'Administrador, Gestor, Consulta, Usuario',
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `Nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaAlta` datetime DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `FechaUltimoAcceso` datetime DEFAULT NULL,
  `Estado_ID` enum('-1','0','1') COLLATE utf8_spanish_ci DEFAULT '-1' COMMENT '-1: DESCONOCIDO, 0: DESCONECTADO, 1: CONECTADO',
  `PasswordVer` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `Origen` varchar(255) COLLATE utf8_spanish_ci DEFAULT 'MANUAL' COMMENT 'Se ha creado de forma MANUAL en la plataforma o desde API de VideoActas',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `Idioma` enum('en','es') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'es',
  `NIF` varchar(20) COLLATE utf8_spanish_ci DEFAULT '',
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `bConfirmado` tinyint(4) NOT NULL DEFAULT 1,
  `URL_Avatar` varchar(500) COLLATE utf8_spanish_ci DEFAULT '',
  `DATA_Avatar` blob DEFAULT NULL,
  `bBaja` tinyint(4) NOT NULL DEFAULT 0,
  `Movil` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Telefono` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `API_ID` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Identificador en API de Usuarios integrada (si procede)',
  `FechaExpiracion` datetime DEFAULT NULL,
  PRIMARY KEY (`Usuario_ID`),
  KEY `Portal_ID_IND` (`Portal_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=2730 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_usuario_backup`
--

DROP TABLE IF EXISTS `tb_acceso_usuario_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_usuario_backup` (
  `Portal_ID` int(11) NOT NULL DEFAULT 1,
  `Usuario_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Login` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Password` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Email` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `RolAcceso_ID` enum('2','3','4','5','6','7') COLLATE utf8_spanish_ci NOT NULL DEFAULT '7' COMMENT 'Administrador, Gestor, Consulta, Usuario',
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `Nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaAlta` datetime DEFAULT NULL,
  `FechaExpiracion` datetime DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `FechaUltimoAcceso` datetime DEFAULT NULL,
  `Estado_ID` enum('-1','0','1') COLLATE utf8_spanish_ci DEFAULT '-1' COMMENT '-1: DESCONOCIDO, 0: DESCONECTADO, 1: CONECTADO',
  `PasswordVer` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `Origen` varchar(255) COLLATE utf8_spanish_ci DEFAULT 'MANUAL',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `Idioma` enum('en','es') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'es',
  `Movil` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `bConfirmado` tinyint(4) NOT NULL DEFAULT 1,
  `URL_Avatar` varchar(500) COLLATE utf8_spanish_ci DEFAULT '',
  `DATA_Avatar` blob DEFAULT NULL,
  `bBaja` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Usuario_ID`),
  KEY `Portal_ID_IND` (`Portal_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_usuario_grupo`
--

DROP TABLE IF EXISTS `tb_acceso_usuario_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_usuario_grupo` (
  `AccesoPortal_ID` int(11) NOT NULL,
  `AccesoUsuario_ID` int(11) NOT NULL,
  `AccesoGrupo_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`AccesoPortal_ID`,`AccesoUsuario_ID`,`AccesoGrupo_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1489 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_usuario_grupo_manager`
--

DROP TABLE IF EXISTS `tb_acceso_usuario_grupo_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_usuario_grupo_manager` (
  `Portal_ID` int(11) NOT NULL DEFAULT 1,
  `AccesoUsuario_ID` int(11) NOT NULL,
  `AccesoGrupo_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`AccesoUsuario_ID`,`AccesoGrupo_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=5461;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_usuario_ldap`
--

DROP TABLE IF EXISTS `tb_acceso_usuario_ldap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_usuario_ldap` (
  `Usuario_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `GrupoUsuario_ID` int(11) DEFAULT 0 COMMENT 'Diputados, Prensa, Ciudadanos, etc...',
  `RolAcceso_ID` int(11) DEFAULT 0 COMMENT 'Supervisor, Gestor, etc...',
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `username` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `userpassword` varchar(50) COLLATE utf8_spanish_ci DEFAULT '',
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `location` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Usuario_ID`),
  KEY `Empresa_ID_IND` (`Portal_ID`),
  KEY `UsuarioRol_ID_IND` (`GrupoUsuario_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=606;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_acceso_usuario_permiso`
--

DROP TABLE IF EXISTS `tb_acceso_usuario_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_acceso_usuario_permiso` (
  `AccesoUsuario_ID` int(11) NOT NULL,
  `AccesoPortal_ID` int(11) NOT NULL DEFAULT 0,
  `AccesoPermiso_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`AccesoUsuario_ID`,`AccesoPortal_ID`,`AccesoPermiso_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=4096 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_canal`
--

DROP TABLE IF EXISTS `tb_canal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_canal` (
  `Canal_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(200) DEFAULT NULL,
  `Directorio` varchar(200) DEFAULT NULL,
  `SolrField` varchar(200) DEFAULT NULL,
  `SolrCore` varchar(200) DEFAULT 'CV_Informativos/',
  `bActivo` tinyint(4) unsigned NOT NULL DEFAULT 0,
  `Orden` int(11) DEFAULT 999,
  PRIMARY KEY (`Canal_ID`),
  KEY `Canal_ID` (`Canal_ID`),
  KEY `Titulo` (`Titulo`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=468 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_cliente`
--

DROP TABLE IF EXISTS `tb_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_cliente` (
  `Cliente_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Email` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Tipo_ID` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `FechaAlta` datetime DEFAULT NULL,
  `NIF` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Telefono` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Poblacion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Municipio_ID` int(11) DEFAULT NULL,
  `Provincia_ID` int(11) DEFAULT NULL,
  `Pais_ID` int(11) DEFAULT NULL,
  `bNewsletter` tinyint(4) unsigned NOT NULL DEFAULT 0,
  `Tutor_ID` int(11) DEFAULT NULL,
  `TutorNIF` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TutorEmail` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TutorNombre` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TutorApellidos` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaBaja` datetime DEFAULT NULL,
  `RazonSocial` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Movil` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CP` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Lat` decimal(25,8) DEFAULT NULL,
  `Lon` decimal(25,8) DEFAULT NULL,
  `Ubicacion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaNacimiento` date DEFAULT NULL,
  `Edad` int(11) unsigned DEFAULT NULL,
  `Sexo_ID` int(11) DEFAULT NULL,
  `ERP_ID` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaImportacion` datetime DEFAULT NULL,
  `Foto1_ID` int(11) DEFAULT NULL,
  `Foto2_ID` int(11) DEFAULT NULL,
  `Galeria1_ID` int(11) DEFAULT NULL,
  `URL` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SSID` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Token` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bConfirmado` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Cliente_ID`),
  UNIQUE KEY `Cliente_ID_IND_U` (`Cliente_ID`),
  KEY `Email` (`Email`),
  KEY `Pais_ID_ind` (`Pais_ID`),
  KEY `Provincia_ID_ind` (`Provincia_ID`),
  CONSTRAINT `tb_cliente_ibfk_1` FOREIGN KEY (`Pais_ID`) REFERENCES `base_pais` (`Pais_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8292 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=193;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_empresa`
--

DROP TABLE IF EXISTS `tb_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_empresa` (
  `Empresa_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Email` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `FechaAlta` datetime DEFAULT NULL,
  `FechaBaja` datetime DEFAULT NULL,
  `Nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CIF` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `RazonSocial` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Telefono` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Movil` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CP` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Poblacion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Municipio_ID` int(11) DEFAULT NULL,
  `Provincia_ID` int(11) DEFAULT NULL,
  `Pais_ID` int(11) DEFAULT NULL,
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Lat` decimal(25,8) DEFAULT NULL,
  `Lon` decimal(25,8) DEFAULT NULL,
  `Ubicacion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaNacimiento` date DEFAULT NULL,
  `Edad` int(11) unsigned DEFAULT NULL,
  `Sexo_ID` int(11) DEFAULT NULL,
  `bNewsletter` tinyint(4) unsigned NOT NULL DEFAULT 0,
  `ERP_ID` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaImportacion` datetime DEFAULT NULL,
  `Foto1_ID` int(11) DEFAULT NULL,
  `Foto2_ID` int(11) DEFAULT NULL,
  `Tipo_ID` int(11) DEFAULT NULL,
  `URL` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SSID` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Token` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bConfirmado` tinyint(4) NOT NULL DEFAULT 1,
  `Galeria1_ID` int(11) DEFAULT NULL,
  `Actividad` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `WebURL` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Twitter` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Facebook` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Empresa_ID`),
  UNIQUE KEY `Cliente_ID_IND_U` (`Empresa_ID`),
  KEY `Email` (`Email`),
  KEY `Pais_ID_ind` (`Pais_ID`),
  KEY `Provincia_ID_ind` (`Provincia_ID`),
  CONSTRAINT `tb_empresa_ibfk_1` FOREIGN KEY (`Pais_ID`) REFERENCES `base_pais` (`Pais_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1170;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_firma_fichero`
--

DROP TABLE IF EXISTS `tb_firma_fichero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_firma_fichero` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Firma_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Video_ID` int(11) DEFAULT NULL,
  `Fichero_ID` int(11) DEFAULT NULL,
  `Recurso_ID` int(11) DEFAULT NULL,
  `Tipo_ID` int(11) DEFAULT NULL COMMENT 'Tipo de fichero: PDF, Subtitulo, Documento, Etc.',
  `Token` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PostEndpoint` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Name` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `MimeType` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `DownloadURL` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Signed` datetime DEFAULT NULL,
  `HashAlgorithm` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Hash` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `StampSign` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FirmaURL` varchar(255) CHARACTER SET utf8 DEFAULT 'etiqsigner://',
  `Base64JSON` text CHARACTER SET utf8 DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `Firmado_XML` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_PDF` blob DEFAULT NULL,
  `Firmado_Documento` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Fecha` datetime DEFAULT NULL,
  `Firmado_Sujeto` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Emisor` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Algoritmo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_ValidoDesde` datetime DEFAULT NULL,
  `Firmado_ValidoHasta` datetime DEFAULT NULL,
  `Firmado_Validez` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`Firma_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=7970;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_firma_fichero_multiple`
--

DROP TABLE IF EXISTS `tb_firma_fichero_multiple`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_firma_fichero_multiple` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `FirmaMultiple_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Firma_ID` int(11) unsigned NOT NULL,
  `Video_ID` int(11) DEFAULT NULL,
  `Recurso_ID` int(11) DEFAULT NULL,
  `Tipo_ID` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `Firmado_XML` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_PDF` blob DEFAULT NULL,
  `Firmado_Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Apellidos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Fecha` datetime DEFAULT NULL,
  `Firmado_Sujeto` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Emisor` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_Algoritmo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Firmado_ValidoDesde` datetime DEFAULT NULL,
  `Firmado_ValidoHasta` datetime DEFAULT NULL,
  `Firmado_Validez` tinyint(4) DEFAULT 1,
  `Firmador_Data` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Signed` datetime DEFAULT NULL,
  PRIMARY KEY (`FirmaMultiple_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_grupo_permiso`
--

DROP TABLE IF EXISTS `tb_grupo_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_grupo_permiso` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Grupo_ID` int(11) NOT NULL,
  `Permiso_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Grupo_ID`,`Permiso_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=52 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_jerarquia`
--

DROP TABLE IF EXISTS `tb_jerarquia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_jerarquia` (
  `Jerarquia_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(200) DEFAULT NULL,
  `Valor` varchar(200) DEFAULT NULL,
  `bActivo` tinyint(4) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`Jerarquia_ID`),
  KEY `Channel_ID` (`Jerarquia_ID`),
  KEY `Name` (`Titulo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=3276 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_legislatura`
--

DROP TABLE IF EXISTS `tb_legislatura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_legislatura` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Legislatura_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SOLR_Field` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `FechaIni` date DEFAULT NULL,
  `FechaFin` date DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Legislatura_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`Legislatura_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1820 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_log_evento`
--

DROP TABLE IF EXISTS `tb_log_evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log_evento` (
  `Evento_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Entidad_ID` int(11) NOT NULL,
  `TipoEntidad_ID` int(11) NOT NULL,
  `Usuario_ID` int(11) NOT NULL COMMENT 'Ingoista o Empresa',
  `Fecha` datetime NOT NULL,
  `TipoEvento_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'Creacion, Borrado, Entrada, Salida, ...',
  `EntidadRel_ID` int(11) DEFAULT NULL,
  `TipoEntidadRel_ID` int(11) DEFAULT NULL,
  `Estado_ID` int(11) DEFAULT NULL,
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Evento_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=40708 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=622 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_ontologia`
--

DROP TABLE IF EXISTS `tb_ontologia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_ontologia` (
  `Ontologia_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `CodigoAPP` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Nivel` int(250) DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Ontologia_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`Ontologia_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6025 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=114 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_orador`
--

DROP TABLE IF EXISTS `tb_orador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_orador` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Orador_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SOLR_Field` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(500) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Nombre` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Apellidos` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `Foto1_ID` int(11) DEFAULT NULL,
  `Foto_URL` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bDuplicado` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Indica si el orador tiene duplicados',
  `bCombinado` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Indica si el orador ha sido combinado',
  `EsAliasDe_Orador_ID` int(11) DEFAULT NULL COMMENT 'Indica si el orador es un alias de un orador con bCombinado',
  PRIMARY KEY (`Orador_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`Orador_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8515 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=174 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_periodo`
--

DROP TABLE IF EXISTS `tb_periodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_periodo` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Periodo_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SOLR_Field` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `FechaIni` date DEFAULT NULL,
  `FechaFin` date DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `TipoPeriodo_ID` varchar(200) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'TIPO_PERIODO_CURSO_ID',
  PRIMARY KEY (`Periodo_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`Periodo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=150 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_permiso_accion`
--

DROP TABLE IF EXISTS `tb_permiso_accion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_permiso_accion` (
  `Portal_ID` int(11) NOT NULL DEFAULT 17,
  `Permiso_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Tipo_ID` int(11) DEFAULT NULL COMMENT 'Descarga, Compartir, etc...',
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Permiso_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`Permiso_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=349 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=115 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_permiso_visualizacion`
--

DROP TABLE IF EXISTS `tb_permiso_visualizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_permiso_visualizacion` (
  `Portal_ID` int(11) NOT NULL DEFAULT 17,
  `Permiso_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SOLR_Field` varchar(255) COLLATE utf8_spanish_ci DEFAULT 'visualizationRights',
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `Tipo_ID` int(11) DEFAULT NULL COMMENT 'Accion, Visualización, etc...',
  `Orden` int(11) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Permiso_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`Permiso_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=3276 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_portal`
--

DROP TABLE IF EXISTS `tb_portal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_portal` (
  `Portal_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `EntornoConfig` int(11) NOT NULL DEFAULT 7,
  `Codigo` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'Portal de video de cliente',
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Protocolo` enum('http://','https://') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'http://',
  `Dominio` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Directorio` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CMS` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'cms.etiqmedia.es',
  `CDN` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'cdn.etiqmedia.es/etiqmedia.es',
  `Canal_ID` varchar(11) COLLATE utf8_spanish_ci NOT NULL DEFAULT '12' COMMENT 'Multiples canales separados por ,',
  `Cliente_ID` int(11) DEFAULT NULL,
  `Foto1_ID` int(11) DEFAULT NULL,
  `LogoRutaWeb` varchar(255) COLLATE utf8_spanish_ci DEFAULT '' COMMENT 'Temporal hasta tener el CMS',
  `AnalyticsCode` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `AnalyticsDomain` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bPrivado` tinyint(4) NOT NULL DEFAULT 0,
  `bActivo` tinyint(4) NOT NULL DEFAULT 0,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `TipoPortal_ID` varchar(200) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'TIPO_PORTAL_INSTITUCIONES_ID',
  `TipoFlujoVideo_ID` varchar(200) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'TIPO_FLUJO_VIDEO_AUTOMATICO_ID',
  `TipoAcceso_ID` varchar(200) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'TIPO_PORTAL_CONFIG_ACCESO_PUBLICO_ID',
  `TipoBuscadorAgrupa_ID` varchar(200) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'TIPO_PORTAL_CONFIG_BUSCADOR_AGRUPA_PS_ID',
  `TipoDeploy_ID` varchar(200) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'TIPO_PORTAL_CONFIG_DEPLOY_REMOTE_ID',
  `TipoVOD_ID` varchar(200) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'TIPO_PORTAL_CONFIG_VOD_LOCAL_ID',
  `FechaAlta` datetime NOT NULL,
  PRIMARY KEY (`Portal_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1191;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_portal_parametro`
--

DROP TABLE IF EXISTS `tb_portal_parametro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_portal_parametro` (
  `Parametro_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Seccion_ID` int(10) unsigned DEFAULT NULL COMMENT 'Home, Buscador, Interactivo, Usuario',
  `TipoValor_ID` varchar(50) NOT NULL DEFAULT 'booleano' COMMENT '''booleano'', ''numero'', ''cadena'', ...',
  `Control_ID` varchar(50) NOT NULL DEFAULT 'check' COMMENT '''check'', ''texto'', ''combo'', ...',
  `Titulo` varchar(255) DEFAULT NULL,
  `CodigoAPP` varchar(100) DEFAULT NULL,
  `Valor` varchar(500) DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Parametro_ID`),
  KEY `ParamTipo_ID` (`Seccion_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1569 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=227 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_portal_widget`
--

DROP TABLE IF EXISTS `tb_portal_widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_portal_widget` (
  `Config_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(10) unsigned NOT NULL DEFAULT 0,
  `CodigoAPP` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT '''html-home-categorias'', ''html-home-izda'', ''html-home-dcha''',
  `Titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `SOLR_Query` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_Exclude` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaIni` datetime DEFAULT NULL,
  `FechaFin` datetime DEFAULT NULL,
  `GenerationURL` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `GenerationData` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `HTML` longtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `Fichero1_ID` int(11) DEFAULT NULL,
  `FechaActualizacion` datetime DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `TagsAMostrar` int(11) DEFAULT 25,
  `TagsMinSize` int(11) DEFAULT 15,
  `TagsMaxSize` int(11) DEFAULT 65,
  `TagsWidth` int(11) DEFAULT 380,
  `TagsHeight` int(11) DEFAULT 190,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Config_ID`),
  UNIQUE KEY `UsuarioRol_ID_IND` (`Config_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1082 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=33390 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_resumen_fragmento`
--

DROP TABLE IF EXISTS `tb_resumen_fragmento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_resumen_fragmento` (
  `Fragmento_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Resumen_ID` int(11) NOT NULL,
  `Portal_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `Usuario_ID` int(11) unsigned NOT NULL,
  `Video_ID` int(11) NOT NULL,
  `Estado_ID` int(11) unsigned NOT NULL DEFAULT 1019 COMMENT 'Pendiente, Procesando, Finalizado, Erroneo',
  `Progreso` float(11,0) NOT NULL DEFAULT 0 COMMENT 'Guardar el time que ha visualizado el usuario desde interactivo',
  `FechaAlta` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Fecha en que agrega a la cola de descargas',
  `FechaIni` datetime DEFAULT NULL COMMENT 'Inicio del procesamiento',
  `FechaFin` datetime DEFAULT NULL COMMENT 'Final del procesamiento',
  `TiempoIni` int(11) DEFAULT NULL COMMENT 'Tiempo desde del trozo de video',
  `TiempoFin` int(11) DEFAULT NULL COMMENT 'Tiempo hasta del trozo de video',
  `Duracion` int(11) DEFAULT NULL COMMENT 'Duracion del trozo de video',
  `Orden` int(11) DEFAULT NULL,
  `OrdenProceso` double(11,4) DEFAULT NULL,
  `Transicion_ID` int(11) DEFAULT NULL COMMENT 'Materia del video de referencia',
  `Titulo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Entradilla` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Thumbnail` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Thumbnail del primer plano incluido en el trozo a descargar',
  `URL_Video` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT 'URL actual',
  `URL_Ponente` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PonenteTitulo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaVisited` datetime DEFAULT NULL,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Fragmento_ID`,`Resumen_ID`,`Portal_ID`,`Usuario_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=657 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1580 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_sesion`
--

DROP TABLE IF EXISTS `tb_sesion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sesion` (
  `Sesion_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Estado_ID` int(11) DEFAULT NULL COMMENT 'Procesado, Validado, Firmado, Publicado',
  `bPublicado` tinyint(4) NOT NULL DEFAULT 0,
  `FechaPublicacion` datetime DEFAULT NULL,
  `bFirmado` tinyint(4) NOT NULL DEFAULT 0,
  `FechaFirma` datetime DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bProcesado` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'bProcesado, ',
  `bValidado` tinyint(4) NOT NULL DEFAULT 0,
  `bAvisado` tinyint(4) NOT NULL DEFAULT 0,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `bDiarioSesiones` tinyint(4) NOT NULL DEFAULT 0,
  `DiarioSesionesPath` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `DiarioSesiones_ID` int(11) DEFAULT NULL COMMENT 'Corresponde a Recurso_ID asociado al borrador en tb_video_recurso',
  `FechaAlta` datetime DEFAULT NULL,
  `FechaEmision` datetime DEFAULT NULL,
  `Firma_HASH` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_id` varchar(256) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `SOLR_videoID` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_title` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_urlVideo` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_urlThumbnail` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_fullUrlVideo` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_dateOfBroadcasting` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_channel` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_validatedBy` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_visualizationRights` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_dateOfValidation` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_dateOfIndexation` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_dateOfPublication` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_published` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_organ` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_agendaItemType` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaImportacion` datetime DEFAULT NULL,
  `Foto1_ID` int(11) DEFAULT NULL,
  `Foto2_ID` int(11) DEFAULT NULL,
  `Codigo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `Tipo_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'Tipo de video',
  `TipoAux_ID` int(11) DEFAULT NULL COMMENT 'Subtipo para categorizar',
  `Organ_ID` int(11) DEFAULT NULL,
  `AgendaItemType_ID` int(11) DEFAULT NULL,
  `Subtitulo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Entradilla` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Descripcion` text CHARACTER SET utf8 DEFAULT NULL,
  `URL_Video` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `URL_Thumbnail` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `Tags` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bDestacado` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Sesion_ID`,`Portal_ID`),
  UNIQUE KEY `Video_ID_IND_U` (`Sesion_ID`),
  KEY `SOLR_ID` (`SOLR_id`(255))
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=2048;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_sesion_grupo`
--

DROP TABLE IF EXISTS `tb_sesion_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sesion_grupo` (
  `Portal_ID` int(11) NOT NULL DEFAULT 17,
  `Sesion_ID` int(11) NOT NULL,
  `Grupo_ID` int(11) NOT NULL,
  PRIMARY KEY (`Sesion_ID`,`Grupo_ID`,`Portal_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_sesion_recurso`
--

DROP TABLE IF EXISTS `tb_sesion_recurso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sesion_recurso` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Recurso_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Sesion_ID` int(11) NOT NULL,
  `Fichero_ID` int(11) NOT NULL,
  `Tipo_ID` int(11) DEFAULT NULL COMMENT 'Tipo de fichero: Imagen, PDF, Subtitulo, Documento, Etc.',
  `Titulo` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 0,
  `Firma_HASH` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bFirmado` tinyint(4) NOT NULL DEFAULT 0,
  `FechaFirma` datetime DEFAULT NULL,
  PRIMARY KEY (`Recurso_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=170 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_sesion_video`
--

DROP TABLE IF EXISTS `tb_sesion_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sesion_video` (
  `Portal_ID` int(11) NOT NULL DEFAULT 17,
  `Sesion_ID` int(11) NOT NULL,
  `Video_ID` int(11) NOT NULL,
  PRIMARY KEY (`Sesion_ID`,`Video_ID`,`Portal_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=2340 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_usuario_favorito`
--

DROP TABLE IF EXISTS `tb_usuario_favorito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario_favorito` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Usuario_ID` int(11) NOT NULL,
  `Favorito_ID` int(11) NOT NULL,
  `Materia_ID` int(11) DEFAULT NULL COMMENT 'Materia o Carpeta libre creada por el usuario para agrupar favoritos',
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Materia o Carpeta libre creada por el usuario para agrupar favoritos',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Usuario_ID`,`Favorito_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_usuario_grupo`
--

DROP TABLE IF EXISTS `tb_usuario_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario_grupo` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Usuario_ID` int(11) NOT NULL,
  `Grupo_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Usuario_ID`,`Grupo_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=129 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_usuario_historial`
--

DROP TABLE IF EXISTS `tb_usuario_historial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario_historial` (
  `Historial_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Usuario_ID` int(11) NOT NULL,
  `Video_ID` int(11) NOT NULL,
  `Estado_ID` int(11) unsigned NOT NULL DEFAULT 1019 COMMENT 'Pendiente, Procesando, Finalizado, Erroneo',
  `Progreso` float(11,0) NOT NULL DEFAULT 0 COMMENT 'Guardar el time que ha visualizado el usuario desde interactivo',
  `FechaAlta` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Fecha en que agrega a la cola de descargas',
  `FechaIni` datetime DEFAULT NULL COMMENT 'Inicio del procesamiento',
  `FechaFin` datetime DEFAULT NULL COMMENT 'Final del procesamiento',
  `TiempoIni` int(11) DEFAULT NULL COMMENT 'Tiempo desde del trozo de video',
  `TiempoFin` int(11) DEFAULT NULL COMMENT 'Tiempo hasta del trozo de video',
  `Duracion` int(11) DEFAULT NULL COMMENT 'Duracion del trozo de video',
  `Orden` int(11) DEFAULT NULL,
  `OrdenProceso` int(11) DEFAULT NULL,
  `Materia_ID` int(11) DEFAULT NULL COMMENT 'Materia del video de referencia',
  `Titulo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Entradilla` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Thumbnail` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Thumbnail del primer plano incluido en el trozo a descargar',
  `URL` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT 'URL actual',
  `FechaVisited` datetime DEFAULT NULL,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Historial_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=233 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=455 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_usuario_notificacion`
--

DROP TABLE IF EXISTS `tb_usuario_notificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario_notificacion` (
  `Notificacion_ID` int(11) NOT NULL,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Usuario_ID` int(11) NOT NULL,
  `Video_ID` int(11) NOT NULL,
  `Materia_ID` int(11) DEFAULT NULL COMMENT 'Materia o Carpeta libre creada por el usuario para agrupar favoritos',
  `Estado_ID` int(11) unsigned NOT NULL DEFAULT 1019 COMMENT 'Pendiente, Procesando, Finalizado, Erroneo',
  `Titulo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Materia o Carpeta libre creada por el usuario para agrupar favoritos',
  `FechaAlta` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Fecha en que agrega a la cola de descargas',
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Usuario_ID`,`Notificacion_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_usuario_permiso`
--

DROP TABLE IF EXISTS `tb_usuario_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario_permiso` (
  `Portal_ID` int(11) NOT NULL DEFAULT 17,
  `Usuario_ID` int(11) NOT NULL,
  `Permiso_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Usuario_ID`,`Permiso_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1638 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_usuario_resumen`
--

DROP TABLE IF EXISTS `tb_usuario_resumen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario_resumen` (
  `Resumen_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Usuario_ID` int(11) NOT NULL,
  `Video_ID` int(11) DEFAULT NULL COMMENT 'Materia o Carpeta libre creada por el usuario para agrupar favoritos',
  `Descarga_ID` int(11) DEFAULT NULL,
  `Estado_ID` int(11) unsigned NOT NULL DEFAULT 1019 COMMENT 'Pendiente, Procesando, Finalizado, Erroneo',
  `Progreso` float(11,0) NOT NULL DEFAULT 0 COMMENT 'Guardar el time que ha visualizado el usuario desde interactivo',
  `FechaAlta` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Fecha de creación',
  `FechaEdited` datetime DEFAULT NULL COMMENT 'Fecha de ultima modificación',
  `FechaGenerated` datetime DEFAULT NULL,
  `FechaIni` datetime DEFAULT NULL COMMENT 'Inicio de edición',
  `FechaFin` datetime DEFAULT NULL COMMENT 'Final del edición',
  `Duracion` int(11) DEFAULT NULL COMMENT 'Duracion del resumen',
  `Tamano` float(11,0) DEFAULT NULL COMMENT 'Tamano en bytes del resumen',
  `Fragmentos` int(11) DEFAULT NULL COMMENT 'Numero de fragmentos que componen el resumen',
  `Titulo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Entradilla` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Thumbnail` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Thumbnail representativo del resumen',
  `URL` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT 'URL de acceso al resumen',
  `FechaVisited` datetime DEFAULT NULL,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bGenerated` tinyint(4) NOT NULL DEFAULT 0,
  `Resolucion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Si está vacio, indica que no se ha obtenido y no se podrá generar los dummies',
  `Transicion_ID` int(11) DEFAULT NULL COMMENT 'Tipo de transición a aplicar entre fragmentos',
  `bDummies` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Si está a 0 indica que todavia no se ha generado los dummies de resolucion apropiada',
  PRIMARY KEY (`Resumen_ID`,`Portal_ID`,`Usuario_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=372 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video`
--

DROP TABLE IF EXISTS `tb_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video` (
  `Video_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Estado_ID` int(11) DEFAULT NULL COMMENT 'Procesado, Validado, Firmado, Publicado',
  `bPublicado` tinyint(4) NOT NULL DEFAULT 0,
  `FechaPublicacion` datetime DEFAULT NULL,
  `bFirmado` tinyint(4) NOT NULL DEFAULT 0,
  `FechaFirma` datetime DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  `bProcesado` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'bProcesado, ',
  `bValidado` tinyint(4) NOT NULL DEFAULT 0,
  `bAvisado` tinyint(4) NOT NULL DEFAULT 0,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `bActaBorrador` tinyint(4) NOT NULL DEFAULT 0,
  `ActaBorradorPath` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ActaBorrador_ID` int(11) DEFAULT NULL COMMENT 'Corresponde a Recurso_ID asociado al borrador en tb_video_recurso',
  `FechaAlta` datetime DEFAULT NULL,
  `FechaEmision` datetime DEFAULT NULL,
  `Firma_HASH` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_id` varchar(256) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `SOLR_videoID` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_title` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_urlVideo` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_urlThumbnail` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_fullUrlVideo` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_dateOfBroadcasting` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_channel` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_validatedBy` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_visualizationRights` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_dateOfValidation` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_dateOfIndexation` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_dateOfPublication` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_published` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_organ` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_agendaItemType` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_speakers` varchar(2000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_summary` varchar(4000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `SOLR_freeStringField2` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Valor de Firma_HASH guardado en SOLR desde video processing',
  `FechaImportacion` datetime DEFAULT NULL,
  `Foto1_ID` int(11) DEFAULT NULL,
  `Foto2_ID` int(11) DEFAULT NULL,
  `Codigo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `Tipo_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'Tipo de video',
  `TipoAux_ID` int(11) DEFAULT NULL COMMENT 'Subtipo para categorizar',
  `Organ_ID` int(11) DEFAULT NULL,
  `AgendaItemType_ID` int(11) DEFAULT NULL,
  `Subtitulo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Entradilla` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Descripcion` text CHARACTER SET utf8 DEFAULT NULL,
  `URL_Video` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `URL_Thumbnail` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `Tags` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bDestacado` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Video_ID`,`Portal_ID`),
  UNIQUE KEY `Video_ID_IND_U` (`Video_ID`),
  KEY `SOLR_ID` (`SOLR_id`(255))
) ENGINE=InnoDB AUTO_INCREMENT=2600 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1422;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_descarga`
--

DROP TABLE IF EXISTS `tb_video_descarga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_descarga` (
  `Descarga_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `Usuario_ID` int(11) NOT NULL,
  `Video_ID` int(11) unsigned NOT NULL,
  `Resumen_ID` int(11) DEFAULT NULL COMMENT 'Si > 0 indica que la descarga es un fragmento de resumen',
  `Estado_ID` int(11) unsigned NOT NULL DEFAULT 1019 COMMENT 'Pendiente, Procesando, Finalizado, Erroneo',
  `Progreso` float(11,0) NOT NULL DEFAULT 0 COMMENT 'bProcesado, ',
  `FechaAlta` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Fecha en que agrega a la cola de descargas',
  `FechaIni` datetime DEFAULT NULL COMMENT 'Inicio del procesamiento',
  `FechaFin` datetime DEFAULT NULL COMMENT 'Final del procesamiento',
  `TiempoIni` int(11) DEFAULT NULL COMMENT 'Tiempo desde del trozo de video',
  `TiempoFin` int(11) DEFAULT NULL COMMENT 'Tiempo hasta del trozo de video',
  `Duracion` int(11) DEFAULT NULL COMMENT 'Duracion del trozo de video',
  `Orden` int(11) DEFAULT NULL,
  `OrdenProceso` double(11,4) DEFAULT NULL,
  `Transicion_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'Tipo de transicion entre videos',
  `Titulo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Entradilla` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Thumbnail` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Thumbnail del primer plano incluido en el trozo a descargar',
  `InputFolderPath` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `InputFileName` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Nombre del NUEVO Fragmento a generar',
  `OutputFolderPath` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `OutputFileName` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Video` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT 'URL (canal/video.mp4) del trozo de video generado',
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `bDownloaded` tinyint(4) NOT NULL DEFAULT 0,
  `bPreloaded` tinyint(4) NOT NULL DEFAULT 0,
  `bPathsChecked` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Descarga_ID`),
  UNIQUE KEY `Descarga_ID_IND_U` (`Descarga_ID`),
  KEY `Portal_Usuario_Video_IND` (`Portal_ID`,`Usuario_ID`,`Video_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1152 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1616;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_eventup`
--

DROP TABLE IF EXISTS `tb_video_eventup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_eventup` (
  `Video_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `Portal_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `PonenteNombre` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PonenteApellidos` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PonenteCargo` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PonenteEmpresa` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Foto1_ID` int(11) DEFAULT NULL COMMENT 'Foto de Ponente',
  `Foto2_ID` int(11) DEFAULT NULL COMMENT 'Logo de empresa',
  `Foto3_ID` int(11) DEFAULT NULL COMMENT 'Para slider en home',
  `Foto4_ID` int(11) DEFAULT NULL,
  `TipoAux_ID` int(11) DEFAULT NULL COMMENT 'Subtipo para categorizar',
  `Tags` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bAgenda` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Video_ID`,`Portal_ID`),
  UNIQUE KEY `Video_ID_IND_U` (`Video_ID`),
  KEY `SOLR_ID` (`PonenteNombre`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=468 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_eventup_clasificacion`
--

DROP TABLE IF EXISTS `tb_video_eventup_clasificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_eventup_clasificacion` (
  `Portal_ID` int(11) NOT NULL DEFAULT 17,
  `Video_ID` int(11) NOT NULL,
  `Tipo_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Video_ID`,`Tipo_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=356 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_favorito`
--

DROP TABLE IF EXISTS `tb_video_favorito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_favorito` (
  `Favorito_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Portal_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `Usuario_ID` int(11) NOT NULL,
  `Video_ID` int(11) unsigned NOT NULL,
  `Estado_ID` int(11) unsigned NOT NULL DEFAULT 1019 COMMENT 'Pendiente, Procesando, Finalizado, Erroneo',
  `Progreso` float(11,0) NOT NULL DEFAULT 0 COMMENT 'Guardar el ultimo time que ha visualizado el usuario desde interactivo',
  `FechaAlta` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Fecha en que agrega a la cola de descargas',
  `FechaIni` datetime DEFAULT NULL COMMENT 'Inicio del procesamiento',
  `FechaFin` datetime DEFAULT NULL COMMENT 'Final del procesamiento',
  `TiempoIni` int(11) DEFAULT NULL COMMENT 'Tiempo desde del trozo de video',
  `TiempoFin` int(11) DEFAULT NULL COMMENT 'Tiempo hasta del trozo de video',
  `Duracion` int(11) DEFAULT NULL COMMENT 'Duracion del trozo de video',
  `Orden` int(11) DEFAULT NULL,
  `OrdenProceso` int(11) DEFAULT NULL,
  `Materia_ID` int(11) DEFAULT NULL COMMENT 'Materia del video de referencia',
  `Titulo` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Entradilla` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL_Thumbnail` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Thumbnail del primer plano incluido en el trozo a descargar',
  `URL_Video` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT 'URL (canal/video.mp4) del trozo de video generado',
  `FechaVisited` datetime(4) DEFAULT NULL,
  `bVisible` tinyint(4) NOT NULL DEFAULT 1,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Favorito_ID`),
  UNIQUE KEY `Descarga_ID_IND_U` (`Favorito_ID`),
  KEY `Portal_Usuario_Video_IND` (`Portal_ID`,`Usuario_ID`,`Video_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=276 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1081 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_fichero`
--

DROP TABLE IF EXISTS `tb_video_fichero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_fichero` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Documento_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Video_ID` int(11) NOT NULL,
  `Fichero_ID` int(11) NOT NULL,
  `Tipo_ID` int(11) DEFAULT NULL COMMENT 'Tipo de fichero: PDF, Subtitulo, Documento, Etc.',
  `Titulo` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Documento_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_grupo`
--

DROP TABLE IF EXISTS `tb_video_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_grupo` (
  `Portal_ID` int(11) NOT NULL DEFAULT 17,
  `Video_ID` int(11) NOT NULL,
  `Grupo_ID` int(11) NOT NULL,
  PRIMARY KEY (`Video_ID`,`Grupo_ID`,`Portal_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=117 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_insight`
--

DROP TABLE IF EXISTS `tb_video_insight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_insight` (
  `Video_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `Portal_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `PonenteNombre` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PonenteApellidos` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PonenteCargo` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PonenteEmpresa` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Foto1_ID` int(11) DEFAULT NULL COMMENT 'Foto de Ponente',
  `Foto2_ID` int(11) DEFAULT NULL COMMENT 'Logo de empresa',
  `Foto3_ID` int(11) DEFAULT NULL COMMENT 'Para slider en home',
  `Foto4_ID` int(11) DEFAULT NULL,
  `TipoAux_ID` int(11) DEFAULT NULL COMMENT 'Subtipo para categorizar',
  `Tags` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bAgenda` tinyint(4) NOT NULL DEFAULT 0,
  `URL_Flickr` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Video_ID`,`Portal_ID`),
  UNIQUE KEY `Video_ID_IND_U` (`Video_ID`),
  KEY `SOLR_ID` (`PonenteNombre`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=564 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_insight_clasificacion`
--

DROP TABLE IF EXISTS `tb_video_insight_clasificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_insight_clasificacion` (
  `Portal_ID` int(11) NOT NULL DEFAULT 17,
  `Video_ID` int(11) NOT NULL,
  `Tipo_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Video_ID`,`Tipo_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=564 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_institucion`
--

DROP TABLE IF EXISTS `tb_video_institucion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_institucion` (
  `Video_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `Portal_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `PonenteNombre` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PonenteApellidos` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PonenteCargo` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PonenteEmpresa` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Foto1_ID` int(11) DEFAULT NULL COMMENT 'Foto de Ponente',
  `Foto2_ID` int(11) DEFAULT NULL COMMENT 'Logo de empresa',
  `Foto3_ID` int(11) DEFAULT NULL COMMENT 'Para slider en home',
  `Foto4_ID` int(11) DEFAULT NULL,
  `TipoAux_ID` int(11) DEFAULT NULL COMMENT 'Subtipo para categorizar',
  `Tags` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bAgenda` tinyint(4) NOT NULL DEFAULT 0,
  `URL_Flickr` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`Video_ID`,`Portal_ID`),
  UNIQUE KEY `Video_ID_IND_U` (`Video_ID`),
  KEY `SOLR_ID` (`PonenteNombre`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=780 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_mentor`
--

DROP TABLE IF EXISTS `tb_video_mentor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_mentor` (
  `Video_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `Portal_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `Titulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `PonenteNombre` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PonenteApellidos` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PonenteCargo` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PonenteEmpresa` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Foto1_ID` int(11) DEFAULT NULL COMMENT 'Foto de Ponente',
  `Foto2_ID` int(11) DEFAULT NULL COMMENT 'Logo de empresa',
  `Foto3_ID` int(11) DEFAULT NULL COMMENT 'Para slider en home',
  `Foto4_ID` int(11) DEFAULT NULL,
  `TipoAux_ID` int(11) DEFAULT NULL COMMENT 'Subtipo para categorizar',
  `Tags` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bAgenda` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Video_ID`,`Portal_ID`),
  UNIQUE KEY `Video_ID_IND_U` (`Video_ID`),
  KEY `SOLR_ID` (`PonenteNombre`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=780 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_mentor_clasificacion`
--

DROP TABLE IF EXISTS `tb_video_mentor_clasificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_mentor_clasificacion` (
  `Portal_ID` int(11) NOT NULL DEFAULT 17,
  `Video_ID` int(11) NOT NULL,
  `Tipo_ID` int(11) NOT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`Video_ID`,`Tipo_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=399 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_orador`
--

DROP TABLE IF EXISTS `tb_video_orador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_orador` (
  `Portal_ID` int(11) NOT NULL DEFAULT 17,
  `Video_ID` int(11) NOT NULL,
  `Orador_ID` int(11) NOT NULL,
  PRIMARY KEY (`Video_ID`,`Orador_ID`,`Portal_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_video_recurso`
--

DROP TABLE IF EXISTS `tb_video_recurso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_video_recurso` (
  `Portal_ID` int(11) NOT NULL DEFAULT 0,
  `Recurso_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Video_ID` int(11) NOT NULL,
  `Fichero_ID` int(11) NOT NULL,
  `Tipo_ID` int(11) DEFAULT NULL COMMENT 'Tipo de fichero: Imagen, PDF, Subtitulo, Documento, Etc.',
  `Titulo` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  `bActivo` tinyint(4) NOT NULL DEFAULT 0,
  `Firma_HASH` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bFirmado` tinyint(4) NOT NULL DEFAULT 0,
  `FechaFirma` datetime DEFAULT NULL,
  PRIMARY KEY (`Recurso_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=429 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=172 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-08 11:49:19
